#ifndef PDP11_ENTRY_H
#define PDP11_ENTRY_H

extern "C" {
	#include <linux/types.h>
	#include <asm/byteorder.h>
}

#include <cstdint>

template<typename pdp_t, typename unsigned_cpu_t, typename signed_cpu_t>
struct pdp11_entry {
	pdp_t __entry;

	unsigned_cpu_t get();
	void set(unsigned_cpu_t entry);

	void inc();
	void dec();
	void inc2();
	void dec2();
	void add(signed_cpu_t summand);
	void add(pdp11_entry summand);
	void sub(signed_cpu_t subtrahend);
	void sub(pdp11_entry subtrahend);

	uint8_t high_bit();	
	uint8_t low_bit();
	uint8_t low_byte();
    
    pdp11_entry rawed();
	pdp11_entry rshifted(unsigned op);
    pdp11_entry lshifted(unsigned op);
    
	uint8_t is_zero();
	uint8_t is_lower_zero();
	uint8_t is_head();
	uint8_t is_signed_max();
	uint8_t is_unsigned_max();
};

typedef pdp11_entry<__le16, uint16_t, int16_t> pdp11_word_t;
typedef pdp11_entry<__u8, uint8_t, int8_t> pdp11_byte_t;

#endif // PDP11_ENTRY_H

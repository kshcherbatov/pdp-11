#ifndef INSTR_SET_INIT_H
#define INSTR_SET_INIT_H

#include <cstdint>
#include "../PDP11.h"

typedef struct {
    uint16_t opcode;
    uint8_t opcode_shift;
    bool has_bit_mode;
    void (*func)(unibus_t *bus);
} emu_func_info_t;

namespace instructionMnemonics {
    enum instr_mnem: uint16_t {
        SWAB, CLR, COM, INC, DEC, NEG, ADC, SBC, TST, ROR, ROL, ASR, ASL, SXT,
        MOV, CMP, ADD, BIC, BIT, BIS,
        XOR, ASH, JSR,
        BR, BNE, BEQ, BPL, BMI, BVC, BVS, BCC, BCS, BGE, BLT, BGT, BLE, BHI, BLOS, BHIS, BLO,
        JMP, RTS, MARK, HALT, WAIT, RTI, RTT,
        INSTRUCTIONS_AMOUNT
    }; //46 instructions
}

typedef enum instructionMnemonics::instr_mnem instr_mnem_t;

struct emulatorFunctions {
    static const uint16_t emu_func_num = instructionMnemonics::INSTRUCTIONS_AMOUNT;
    emu_func_info_t info[emu_func_num];
    void instructionSetInit(void **instr_set, void (*default_func)());
};

#define emulatorFunctionsInit(emu_func) do { \
    using namespace instructionMnemonics; \
    emu_func.info[SWAB] = {0003, 6, false, EMU_CALLBACK(_SWAB)}; \
    emu_func.info[CLR]  = {0050, 6, true,  EMU_CALLBACK(_CLR)}; \
    emu_func.info[COM]  = {0051, 6, true,  EMU_CALLBACK(_COM)}; \
    emu_func.info[INC]  = {0052, 6, true,  EMU_CALLBACK(_INC)}; \
    emu_func.info[DEC]  = {0053, 6, true,  EMU_CALLBACK(_DEC)}; \
    emu_func.info[NEG]  = {0054, 6, true,  EMU_CALLBACK(_NEG)}; \
    emu_func.info[ADC]  = {0055, 6, true,  EMU_CALLBACK(_ADC)}; \
    emu_func.info[SBC]  = {0056, 6, true,  EMU_CALLBACK(_SBC)}; \
    emu_func.info[TST]  = {0057, 6, true,  EMU_CALLBACK(_TST)}; \
    emu_func.info[ROR]  = {0060, 6, true,  EMU_CALLBACK(_ROR)}; \
    emu_func.info[ROL]  = {0061, 6, true,  EMU_CALLBACK(_ROL)}; \
    emu_func.info[ASR]  = {0062, 6, true,  EMU_CALLBACK(_ASR)}; \
    emu_func.info[ASL]  = {0063, 6, true,  EMU_CALLBACK(_ASL)}; \
    emu_func.info[SXT]  = {0067, 6, false, EMU_CALLBACK(_SXT)}; \
    emu_func.info[MOV]  = {001, 12, true,  EMU_CALLBACK(_MOV)}; \
    emu_func.info[CMP]  = {002, 12, true,  EMU_CALLBACK(_CMP)}; \
    emu_func.info[ADD]  = {006, 12, true,  EMU_CALLBACK(_ADD)}; \
    emu_func.info[BIT]  = {003, 12, true,  EMU_CALLBACK(_BIT)}; \
    emu_func.info[BIC]  = {004, 12, true,  EMU_CALLBACK(_BIC)}; \
    emu_func.info[BIS]  = {005, 12, true,  EMU_CALLBACK(_BIS)}; \
    emu_func.info[XOR]  = {074,  9, false, EMU_CALLBACK(_XOR)}; \
    emu_func.info[ASH]  = {072,  9, false, EMU_CALLBACK(_ASH)}; \
    emu_func.info[JSR]  = {004,  9, false, EMU_CALLBACK(_JSR)}; \
    emu_func.info[BR]   = {0b00000001, 8, false, EMU_CALLBACK(_BR)}; \
    emu_func.info[BNE]  = {0b00000010, 8, false, EMU_CALLBACK(_BNE)}; \
    emu_func.info[BEQ]  = {0b00000011, 8, false, EMU_CALLBACK(_BEQ)}; \
    emu_func.info[BPL]  = {0b10000000, 8, false, EMU_CALLBACK(_BPL)}; \
    emu_func.info[BMI]  = {0b10000001, 8, false, EMU_CALLBACK(_BMI)}; \
    emu_func.info[BVC]  = {0b10000100, 8, false, EMU_CALLBACK(_BVC)}; \
    emu_func.info[BVS]  = {0b10000101, 8, false, EMU_CALLBACK(_BVS)}; \
    emu_func.info[BCC]  = {0b10000110, 8, false, EMU_CALLBACK(_BCC)}; \
    emu_func.info[BCS]  = {0b10000111, 8, false, EMU_CALLBACK(_BCS)}; \
    emu_func.info[BGE]  = {0b00000100, 8, false, EMU_CALLBACK(_BGE)}; \
    emu_func.info[BLT]  = {0b00000101, 8, false, EMU_CALLBACK(_BLT)}; \
    emu_func.info[BGT]  = {0b00000110, 8, false, EMU_CALLBACK(_BGT)}; \
    emu_func.info[BLE]  = {0b00000111, 8, false, EMU_CALLBACK(_BLE)}; \
    emu_func.info[BHI]  = {0b10000010, 8, false, EMU_CALLBACK(_BHI)}; \
    emu_func.info[BLOS] = {0b10000011, 8, false, EMU_CALLBACK(_BLOS)}; \
    emu_func.info[BHIS] = {0b10000110, 8, false, EMU_CALLBACK(_BHIS)}; \
    emu_func.info[BLO]  = {0b10000111, 8, false, EMU_CALLBACK(_BLO)}; \
    emu_func.info[JMP]  = {01,  6, false, EMU_CALLBACK(_JMP)}; \
    emu_func.info[MARK] = {064, 6, false, EMU_CALLBACK(_MARK)}; \
    emu_func.info[RTS]  = {020, 3, false, EMU_CALLBACK(_RTS)}; \
    emu_func.info[HALT] = {0,   0, false, EMU_CALLBACK(_HALT)}; \
    emu_func.info[WAIT] = {1,   0, false, EMU_CALLBACK(_WAIT)}; \
    emu_func.info[RTI]  = {2,   0, false, EMU_CALLBACK(_RTI)}; \
    emu_func.info[RTT]  = {6,   0, false, EMU_CALLBACK(_RTT)}; \
} while(0)

#endif /* INSTR_SET_INIT_H */

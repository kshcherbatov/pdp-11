#include <assert.h>
#include <cstring>

#include "PDP11Pic.h"
#include "pdp11_unibus.h"
#include "pdp11_pipeline.h"
#include "PDP11.h"
#include "initializers.h"
#include "instruction_set/instr_funcs.h"
#include "instruction_set/disasm_funcs.h"

unsigned char *_MEMORY_BEGIN = 0;

namespace Emulator {
    namespace State {
        enum {
            Running,
            isWaitingForInterrupt,
            Halted
        };
    }
}

using namespace Emulator;

static const uint32_t Pixel_map[0x100] __attribute__ ((aligned (0x10))) = PIXELMAP_INITIALIZER;

static void wordset( pdp11_word_t * __restrict__ dest, uint16_t value, size_t size ) {
	for( uintptr_t i = 0; i < size; i++ )
		dest[i].set(value);
}

static int memory_model_init(unibus_t *bus, const char *ROM_dump_path) {
#ifndef NDEBUG
    wordset(bus->R, DEAD_WORD, registerAmount);
    memset(bus->ROM,  0, ROM_size);
    memset(bus->RAM,  0, RAM_size);
    memset(bus->VRAM, 0, VRAM_size);
	bus->keyboard_buffer.set(DEAD_WORD);
#endif /* NDEBUG */

	/* Graphics test */
//	unsigned char vram_buf[] = VRAM_INITIALIZER;
//	memcpy(bus->VRAM, vram_buf, PDP11_VRAM_SIZE);
	/* End of graphics test */

    bus->SP.set(bus->RAM - bus->memory + RAM_size - 6*sizeof(pdp11_word_t));
    bus->PC.set(0);
	bus->ps_word.flags.set(0);
    bus->ps_word.I = 0b11000000000;
    bus->set_high_CPU_priority();
    bus->interrupt_vector_PS = bus->ps_word.flags;
    bus->interrupt_vector_PC.set(0400);

	FILE *ROM_dump = fopen(ROM_dump_path, "rb");
	if(ROM_dump == NULL)
		return EIO;

	for(ptrdiff_t i = 0; fread(bus->ROM + i, sizeof(pdp11_word_t), 1, ROM_dump); i += sizeof(pdp11_word_t));

	if(ferror(ROM_dump))
		return EIO;

	if(fclose(ROM_dump) == -1)
		return EIO;

    _MEMORY_BEGIN = bus->memory;
	return 0;
}

PDP11::PDP11(const char *ROM_dump_path, bool pipelining_enabled, bool cache_enabled) :
    interruptController(new pdp11_pic()),
    pipeline(new pdp11_pipeline(pipelining_enabled, cache_enabled)),
    state(State::Running),
    display({0200*2, 0200*2})
{
    QWriteLocker lock(&stateMutex);
    
	unibus = (unibus_t *)malloc(sizeof(*unibus));
	if( unibus == NULL ) {
		throw ENOMEM;
	}

	int err = memory_model_init(unibus, ROM_dump_path);
	if( err )
		throw err;
    
    emu_funcs_init(instr_set);
	disasm_funcs_init(disasm_set);
}

PDP11::~PDP11() {
    QWriteLocker lock(&stateMutex);
	free(unibus);
    delete interruptController;
    delete pipeline;
}

static const char *instr_strerror(instr_ret_code_t err) {
	switch(err) {
		case INSTR_CODE_SUCCESS:
			return "success";
		case INSTR_CODE_HALT:
			return "halt";
		case INSTR_CODE_WANT_WAIT:
			return "want wait";
		case INSTR_CODE_NOT_REALISED:
			return "function is not realised";
		case INSTR_CODE_INVALID_MODE:
			return "invalid addressing mode exception";
		case INSTR_CODE_DIVISION_BY_ZERO:
			return "division by zero exception;";
		case INSTR_CODE_INVALID_ADDRESS:
        return "invalid address exception";
		case INSTR_CODE_STACK_OVERFLOW:
			return "stack overflow exception";
		case INSTR_CODE_EMPTY_STACK:
			return "empty stack exception";
	}
    assert(!"BUG");
    return "";
}

int PDP11::stepi() {
    using namespace State;

    QWriteLocker lock(&stateMutex);

    if (state == Halted)
        return Status::Halt; /* might not happen */

    if (state == isWaitingForInterrupt && !interruptController->interruptRequested)
        return Status::Wait;

    if (unibus->CPU_priority_is_low() && interruptController->interruptRequested) {
        unibus->set_high_CPU_priority();
        state = Running;
        unibus->stack_push(unibus->ps_word.flags);
        unibus->stack_push(unibus->PC);
        unibus->ps_word.flags = unibus->interrupt_vector_PS;
        unibus->PC = unibus->interrupt_vector_PC;
        
        int res = interruptController->pop();
        fprintf(stderr, "pop value from controller %d\n", res);
        fprintf(stderr, "intPC: %d\n", unibus->interrupt_vector_PC.get());
        
        unibus->keyboard_buffer.set(res);
        return Status::Success;
    }

	uint16_t instr = unibus->host_addr_get(unibus->PC)->get();
	emu_func_t *emu_func = (emu_func_t *)instr_set[instr];
	instr_ret_code_t res = emu_func(unibus, pipeline);

	switch(res) {
		case INSTR_CODE_SUCCESS:
            return Status::Success;
            
		case INSTR_CODE_HALT:
            state = Halted;
            return Status::Halt;
            
		case INSTR_CODE_WANT_WAIT:
            state = isWaitingForInterrupt;
            unibus->set_low_CPU_priority();
            return Status::Success;
            
		default: /* exceptions */
            state = Halted;
			fprintf(stderr, "Instruction %o: %s\n", instr, instr_strerror(res));
            return Status::Failed;
	}
}

void PDP11::interruptSwitchContext() {
    QWriteLocker lock(&stateMutex);
}

void PDP11::makeDisasm(QStringList &disasmList)
{
    QReadLocker lock(&stateMutex);
    unibus->PC.set(0);

    const int bytesToDisasm = 1024;
    while ( unibus->PC.get() < bytesToDisasm ) {
        uint16_t old_PC = unibus->PC.get();
        
        try {
            uint16_t instr = unibus->host_addr_get(unibus->PC)->get();
            disasm_func_t *disasm_func = (disasm_func_t *)disasm_set[instr];
            disasmList.append(disasm_func(unibus));
        }
        catch(instr_ret_code_t err) {
            QString errorMsg = QString("Disasm error %1").arg(err);
            disasmList.append(errorMsg);
        }

        int dataWords = unibus->PC.get() - old_PC - instructionSize;
        
        for (int i = 0; i < dataWords; i += instructionSize) {
            disasmList.append(DATA_WORD_PLACEHOLDER + 
                              QString(" (0%1)").arg(unibus->ROM[old_PC + instructionSize + i], 0,
                              GUI_NUMBERS_BASE, QChar('0')));
        }
    }

    unibus->PC.set(0);
}

const unsigned char *PDP11::VRAM_get() {
    QReadLocker lock(&stateMutex);
    
	const unsigned char * __restrict__ vram = unibus->VRAM;
    uint32_t *__restrict__ fake_vram = fakeVRAM;

#if __GNUC__ > 4
#pragma omp parallel for simd aligned(Pixel_map, fake_vram : 16)
#else
#pragma omp parallel for
#endif
    for(ptrdiff_t i = 0; i < VRAM_size; i++) {
		fake_vram[i] = Pixel_map[vram[i]];
	}

    return (unsigned char *)fakeVRAM;
}

void PDP11::feedCharQueue(int c) {
    interruptController->push(c);
}

int16_t PDP11::Rn_get(uintptr_t n) {
    assert(n <= registerAmount);
    QReadLocker lock(&stateMutex);
	return unibus->R[n].get();
}

uint16_t PDP11::flag_get(char flagname) {
    QReadLocker lock(&stateMutex);
    
	switch (flagname) {
		case 'C':
            return unibus->ps_word.C;
		case 'V':
            return unibus->ps_word.V;
		case 'Z':
            return unibus->ps_word.Z;
		case 'N':
            return unibus->ps_word.N;
		case 'T':
            return unibus->ps_word.T;
		case 'I':
            return unibus->ps_word.I;
		default:
			assert(!"flag_get: bad flagname!");
	}
}

uint16_t PDP11::flags_get() {
    QReadLocker lock(&stateMutex);
    
	return unibus->ps_word.flags.get();
}

void PDP11::restart(const char *ROM_dump_path) {
    QWriteLocker lock(&stateMutex);

    state = State::Running;
	memory_model_init(unibus, ROM_dump_path);
    interruptController->reset();

    pipeline->restart();
}

void PDP11::restartPipeline(bool pipelining_enabled, bool cache_enabled)
{
    pipeline->restart(pipelining_enabled, cache_enabled);
}

uint64_t PDP11::getClockNumber() {
    return pipeline->getTotalTime();
}

QString snum_QString(int16_t word) {
	if ((int16_t)word < 0) {
		return QString("-0%1").arg((int16_t)-word, 0,
                                   GUI_NUMBERS_BASE, QChar('0'));
	} else {
		return QString("0%1").arg(word & 0xFFFF, 0,
                                  GUI_NUMBERS_BASE, QChar('0'));
	}
}

#ifndef __PIPELINE_TABLE_H
#define __PIPELINE_TABLE_H

#include <cstdint>
#include <unordered_map>

struct InstructionDescription {
    static const uint32_t maxMemoryAccessNumber = 20;
	uint64_t f_count;
	uint64_t d_count;
	uint64_t l_count;
	uint64_t e_count;
	uint64_t s_count;
	uint32_t loadNumber;
	uint32_t storeNumber;
	void *loadAddresses[maxMemoryAccessNumber];
	void *storeAddresses[maxMemoryAccessNumber];
};

typedef uint64_t tick_t;

typedef std::unordered_map<void *, tick_t> ExtentMap;

class PipelineTable {
	tick_t f_free;
	tick_t d_free;
	tick_t l_free;
	tick_t e_free;
	tick_t s_free;
	ExtentMap hazardAddresses;

	tick_t getLoadStoreBoundary(void *address);
public:
	PipelineTable(): f_free(0), d_free(0), l_free(0), e_free(0), s_free(0) {}
	void addRow(const InstructionDescription &instr);
    void reset() {f_free = d_free = l_free = e_free = s_free;}
    void restart();
	uint64_t getTotalTime() {return s_free;}
};

#endif

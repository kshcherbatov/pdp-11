#include "PipelineTable.h"

#define max(x,y) ({ \
	typeof(x) _x = x; \
	typeof(y) _y = y; \
	_x > _y ? _x : _y; \
})

tick_t PipelineTable::getLoadStoreBoundary(void * address) {
	tick_t result = 0;
	ExtentMap::const_iterator got = hazardAddresses.find(address);
	if(got != hazardAddresses.end()) {
		if(got->second < f_free)
			hazardAddresses.erase(got); /* unnesessary cleaning */
		else result = got->second;
	}
	return result;
}

void PipelineTable::addRow(const InstructionDescription &instr) {
	f_free = max(f_free + instr.f_count, d_free); /* Use OLD f_free in RHS */
	d_free = max(f_free + instr.d_count, l_free);
	for(uintptr_t i = 0; i < instr.loadNumber; i++)
		d_free = max(d_free, getLoadStoreBoundary(instr.loadAddresses[i]));
	l_free = max(d_free + instr.l_count, e_free);
	e_free = max(l_free + instr.e_count, s_free);
	s_free = e_free + instr.s_count;
	for(uintptr_t i = 0; i < instr.storeNumber; i++)
        hazardAddresses[instr.storeAddresses[i]] = s_free;
}

void PipelineTable::restart()
{
    f_free = d_free = l_free = e_free = s_free = 0;
    hazardAddresses.clear();
}

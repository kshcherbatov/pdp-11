#ifndef DISASM_FUNCS_H
#define DISASM_FUNCS_H

#include <QString>
#include "../PDP11.h"

typedef QString disasm_func_t(unibus_t *bus);

void disasm_funcs_init(void **instr_set);

#endif /* DISASM_FUNCS_H */

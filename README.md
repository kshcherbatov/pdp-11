![Screenshot from 2017-01-25 10-55-21.png](https://bitbucket.org/repo/7q7jkd/images/3554425091-Screenshot%20from%202017-01-25%2010-55-21.png)# PDP-11

**PDP-11 Emulator**, homework project by Neganov ALexey and Shcherbatov Kirill.

### Environment:
Linux

QtCreator (https://www.qt.io/ide/)

### Links:
https://en.wikipedia.org/wiki/PDP-11_architecture

http://research.microsoft.com/en-us/um/people/gbell/Digital/PDP%2011%20Handbook%201969.pdf

How to set up CPU FLAGS:
https://programmer209.wordpress.com/2011/08/03/the-pdp-11-assembly-language/

### License
PDP-11 Emulator is [MIT-licensed](LICENSE)
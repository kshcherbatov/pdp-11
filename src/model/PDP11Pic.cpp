#include <cstring>
#include <qnamespace.h>

#include "PDP11Pic.h"

enum pdp11_key {
    PDP11_Key_NUL = 0,
    PDP11_Key_Shift,
    PDP11_Key_CapsLock,
    PDP11_Key_A,
    PDP11_Key_B,
    PDP11_Key_C,
    PDP11_Key_D,
    PDP11_Key_E,
    PDP11_Key_F,
    PDP11_Key_G,
    PDP11_Key_H,
    PDP11_Key_I,
    PDP11_Key_J,
    PDP11_Key_K,
    PDP11_Key_L,
    PDP11_Key_M,
    PDP11_Key_N,
    PDP11_Key_O,
    PDP11_Key_P,
    PDP11_Key_Q,
    PDP11_Key_R,
    PDP11_Key_S,
    PDP11_Key_T,
    PDP11_Key_U,
    PDP11_Key_V,
    PDP11_Key_W,
    PDP11_Key_X,
    PDP11_Key_Y,
    PDP11_Key_Z,
    PDP11_Key_0,
    PDP11_Key_1,
    PDP11_Key_2,
    PDP11_Key_3,
    PDP11_Key_4,
    PDP11_Key_5,
    PDP11_Key_6,
    PDP11_Key_7,
    PDP11_Key_8,
    PDP11_Key_9,
    PDP11_Key_Period,
    PDP11_Key_Colon,
    PDP11_Key_Comma,
    PDP11_Key_Semicolon,
    PDP11_Key_ParenLeft,
    PDP11_Key_Asterisk,
    PDP11_Key_Exclam,
    PDP11_Key_Question,
    PDP11_Key_BraceRight,
    PDP11_Key_AsciiCircum,
    PDP11_Key_ParenRight,
    PDP11_Key_NumberSign,
    PDP11_Key_Dollar,
    PDP11_Key_BraceLeft,
    PDP11_Key_Percent,
    PDP11_Key_Caret,
    PDP11_Key_Ampersand,
    PDP11_Key_Minus,
    PDP11_Key_Plus,
    PDP11_Key_At,
    PDP11_Key_Space,
    PDP11_Key_Amount
};

pdp11_pic::pdp11_pic(): interruptRequested(false), queue() {
    memset(scancode_convertion_table, PDP11_Key_NUL, 1 << CHAR_BIT);

    using namespace Qt;
#define INIT_KEY(key) \
    (scancode_convertion_table[scancodeIndex(key)] = PDP11_ ## key)

    INIT_KEY(Key_Shift);
    INIT_KEY(Key_CapsLock);
    INIT_KEY(Key_A);
    INIT_KEY(Key_B);
    INIT_KEY(Key_C);
    INIT_KEY(Key_D);
    INIT_KEY(Key_E);
    INIT_KEY(Key_F);
    INIT_KEY(Key_G);
    INIT_KEY(Key_H);
    INIT_KEY(Key_I);
    INIT_KEY(Key_J);
    INIT_KEY(Key_K);
    INIT_KEY(Key_L);
    INIT_KEY(Key_M);
    INIT_KEY(Key_N);
    INIT_KEY(Key_O);
    INIT_KEY(Key_P);
    INIT_KEY(Key_Q);
    INIT_KEY(Key_R);
    INIT_KEY(Key_S);
    INIT_KEY(Key_T);
    INIT_KEY(Key_U);
    INIT_KEY(Key_V);
    INIT_KEY(Key_W);
    INIT_KEY(Key_X);
    INIT_KEY(Key_Y);
    INIT_KEY(Key_Z);
    INIT_KEY(Key_0);
    INIT_KEY(Key_1);
    INIT_KEY(Key_2);
    INIT_KEY(Key_3);
    INIT_KEY(Key_4);
    INIT_KEY(Key_5);
    INIT_KEY(Key_6);
    INIT_KEY(Key_7);
    INIT_KEY(Key_8);
    INIT_KEY(Key_9);
    INIT_KEY(Key_Period);
    INIT_KEY(Key_Colon);
    INIT_KEY(Key_Comma);
    INIT_KEY(Key_Semicolon);
    INIT_KEY(Key_ParenLeft);
    INIT_KEY(Key_Asterisk);
    INIT_KEY(Key_Exclam);
    INIT_KEY(Key_Question);
    INIT_KEY(Key_BraceRight);
    INIT_KEY(Key_AsciiCircum);
    INIT_KEY(Key_ParenRight);
    INIT_KEY(Key_NumberSign);
    INIT_KEY(Key_Dollar);
    INIT_KEY(Key_BraceLeft);
    INIT_KEY(Key_Percent);
    INIT_KEY(Key_Ampersand);
    INIT_KEY(Key_Minus);
    INIT_KEY(Key_Plus);
    INIT_KEY(Key_At);
    INIT_KEY(Key_Space);
#undef INIT_KEY
};

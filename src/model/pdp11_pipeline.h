#ifndef PDP11_PIPELINE_H
#define PDP11_PIPELINE_H

#include <cstdint>
#include <cassert>

#include "instruction_set/instr_set_init.h"
#include "pdp11_unibus.h"
#include "PDP11.h"
#include "PipelineTable.h"
#include "LRU_cache.h"

struct MemoryAccessCtx {
    addr_mode_t mode;
    void *address;
};

class InstructionCtx {
    friend class pdp11_pipeline;
    instr_mnem_t operation;
    uint32_t loadNumber;
    MemoryAccessCtx loadCtx[InstructionDescription::maxMemoryAccessNumber];
    uint32_t storeNumber;
    MemoryAccessCtx storeCtx[InstructionDescription::maxMemoryAccessNumber];
public:
    InstructionCtx(instr_mnem_t opcode) : operation(opcode), loadNumber(0), storeNumber(0) {}
    void addLoadByteCtx(addr_mode_t mode, void *address);
    void addLoadWordCtx(addr_mode_t mode, void *address);
    void addStoreByteCtx(addr_mode_t mode, void *address);
    void addStoreWordCtx(addr_mode_t mode, void *address);
};

class pdp11_pipeline {
    bool enabled;
    PipelineTable table; /* used if pipelining is enabled */
    uint64_t totalTime; /* used if pipelining is not enabled */
    const uint64_t fetchTime;
    const uint64_t decodeTime;
    uint64_t loadTime[Emulator::registerAmount];
    uint64_t loadCacheTime[Emulator::registerAmount];
    uint64_t execTime[instructionMnemonics::INSTRUCTIONS_AMOUNT];
    uint64_t storeTime[Emulator::registerAmount];
    uint64_t storeCacheTime[Emulator::registerAmount];
    InstructionDescription currentInstructionDescription;
    bool cache_enabled;
    LRU_cache cache;
public:
    pdp11_pipeline(bool is_enabled, bool cache_is_enabled);
    void evaluate(const InstructionCtx &ctx);
    void reset();
    void restart(bool is_enabled, bool cache_is_enabled);
    void restart(void);
    uint64_t getTotalTime() {return enabled ? table.getTotalTime() : totalTime;}
}; /* pipeline_t */

#endif // PDP11_PIPELINE_H

#-------------------------------------------------
#
# Project created by QtCreator 2016-10-20T13:57:22
#
#-------------------------------------------------

QT       += core gui
#CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PDP-11
TEMPLATE = app

QMAKE_CXXFLAGS_WARN_OFF = -Wno-unused-parameter
QMAKE_CXXFLAGS += -isystem $$[QT_INSTALL_HEADERS] -fopenmp -O0 -std=gnu++11 -Wall -Wextra -Woverflow -Wuninitialized -fstrict-aliasing -Wstrict-aliasing -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wunsafe-loop-optimizations -Wcast-qual -Wparentheses -Wsign-compare -Winline -Wdisabled-optimization
QMAKE_LIBS += -lgomp -lpthread

SOURCES += main.cpp\
        mainwindow.cpp \
    model/instruction_set/instr_set_init.cpp \
    model/instruction_set/instr_funcs.cpp \
    model/instruction_set/disasm_funcs.cpp \
    model/pdp11_entry.cpp \
    model/pdp11_unibus.cpp \
    model/PDP11.cpp \
    model/PDP11Pic.cpp \
    model/pdp11_pipeline.cpp \
    model/PipelineTable.cpp \
    model/LRU_cache.cpp

HEADERS  += mainwindow.h \
    model/initializers.h \
    model/instruction_set/instr_set_init.h \
    model/instruction_set/instr_funcs.h \
    model/instruction_set/disasm_funcs.h \
    model/pdp11_entry.h \
    model/pdp11_unibus.h \
    model/PDP11.h \
    model/PDP11Pic.h \
    model/pdp11_pipeline.h \
    model/PipelineTable.h \
    model/LRU_cache.h

FORMS    += mainwindow.ui

DISTFILES +=

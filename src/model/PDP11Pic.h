#ifndef PDP11_PIC_H
#define PDP11_PIC_H

#include <cstdint>
#include <climits>
#include <stdio.h>

class pdp11_pic {
    static int8_t scancodeSign(uint32_t key) {
        return ((!(key >> 24))*2 - 1);
    }

    static unsigned char scancodeIndex(uint32_t key) {
        return ((unsigned char)(scancodeSign(key)*(key & 0xff)));
    }

    int8_t scancode_convertion_table[1 << CHAR_BIT];

public:
    bool interruptRequested;

    pdp11_pic();

    void push(int scancode) {
        queue.push(scancode);
        interruptRequested = true;
    }

    int8_t pop() {
        int8_t result = queue.pop();
        if(queue.head == 0)
            interruptRequested = false;
        return result;
    }
    
    void reset() {
        queue.reset();
        interruptRequested = false;
    }

    class queue {
        const uint32_t head_mask[5] = {0, 0xff, 0xff00, 0xff0000, 0xff000000};
        uint32_t data;
        uint8_t head;
        int8_t pop() {
            int8_t result = (data & head_mask[head]) >> ((head-1)*8);
            if(head !=0 )
                head--;
            fprintf(stderr, "controller poping: head=%x, data=%x, result=%x\n", head, data, result);
            return result;
        }
        void push(int8_t scancode) {
            data <<= 8;
            if(head < 4)
                head++;
            data |= scancode;
            fprintf(stderr, "controller pushing: head=%x, data=%x\n", head, data);
        }
        void reset() {
            head = 0;
        }
        friend void   pdp11_pic::push(int scancode);
        friend int8_t pdp11_pic::pop();
        friend void pdp11_pic::reset();
    public:
        queue(): head(0) {}
    } queue;
}; /* pic_t */ 

#endif // PDP11_PIC_H

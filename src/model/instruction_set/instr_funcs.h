#ifndef INSTR_FUNCS_H
#define INSTR_FUNCS_H

#include <cstdint>
#include <stddef.h>
#include "../pdp11_unibus.h"
#include "../PDP11.h"

typedef enum {
    INSTR_CODE_SUCCESS          =  0,
    INSTR_CODE_HALT             =  1,
    INSTR_CODE_WANT_WAIT        =  2,
    INSTR_CODE_NOT_REALISED     = -1,
    INSTR_CODE_INVALID_MODE     = -2,
    INSTR_CODE_DIVISION_BY_ZERO = -3,
    INSTR_CODE_INVALID_ADDRESS  = -4,
    INSTR_CODE_STACK_OVERFLOW   = -5,
    INSTR_CODE_EMPTY_STACK      = -6
} instr_ret_code_t;

typedef instr_ret_code_t emu_func_t(unibus_t *bus, pipeline_t *pipeline);

void emu_funcs_init(void **instr_set);

/*
  15       11  10   6  5  3  2 	    0
   B 0 0 0  1  Opcode  Mode  Register
*/
typedef union {
	struct {
	__le16 reg  : 3;
	__le16 mode : 3;
	__le16 cmd  : 10;
	} __attribute__((packed));
	pdp11_word_t word;
} single_op_instr_t;

/*
	15 	14  12 	11 9 	8    6 	5  3 	2	  0
	B 	Opcode 	Mode 	Source 	Mode 	Destination
*/

typedef union {
	struct {
		__le16 num2  : 3;
		__le16 mode2 : 3;
		__le16 reg1  : 3;
		__le16 mode1 : 3;
		__le16 cmd   : 4;
	} __attribute__((packed));
	pdp11_word_t word;
} double_op_instr_t;

/*
    15  14 12  11   9    8      6  5  3    2     0
    0   1 1 1  Opcode    Register  Mode    Src/Dest
*/

typedef union {
	struct {
		__le16 num2  : 3;
		__le16 mode2 : 3;
        __le16 reg1  : 3;
		__le16 cmd   : 7;
	} __attribute__((packed));
	pdp11_word_t word;
} doubleR_op_instr_t;

/*
	15   8  7    0
 	Opcode  Offset
*/

typedef union {
	struct {
		int16_t offset : 8;
		int16_t cmd    : 8;
    } __attribute__((packed));
	pdp11_word_t word;
} branch_op_instr_t;


#endif /* INSTR_FUNCS_H */

#include <assert.h>
#include <QShortcut>
#include <QVector>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "model/PDP11.h"

#define ROM_dump_path "../data/ROM"

static QMap<int, QString> pdp11StateMsgs = {
    {GUI_STATE_FINISHED_INSTRUCTION, "PDP11 CPU finished instruction"},
    {GUI_STATE_WAIT, "PDP11 CPU is waiting"},
    {GUI_STATE_HALT, "PDP11 CPU halted"},
    {GUI_STATE_INITIAL, "PDP11 CPU is in initial state"},
    {GUI_STATE_RUNNING, "PDP11 CPU is running"},
    {GUI_STATE_RUN_FINISHED, "PDP11 CPU stopped"},
    {GUI_STATE_RUNNING_INSTUCTION, "PDP11 CPU is running instruction"},
    {GUI_STATE_LAST_INSTRUCTION_FAILED, "PDP11 CPU failed running last instruction"},
    {GUI_STATE_CHANGED_PIPELINE_CONFIG, "PDP11 pipeline config changed. Caches & total time were cleaned."},
    {GUI_STATE_CHANGED_FASTMODE_CONFIG, "PDP11 fastmode config changed"},
    {GUI_STATE_CHANGED_CACHE_CONFIG, "PDP11 caching config changed.  Caches & total time were cleaned."},
    {GUI_STATE_NO_MSG, ""}
};

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
    pipelineEnabled(true),
    cacheEnabled(true),
    pdpHandle(new Emulator::PDP11(ROM_dump_path, pipelineEnabled, cacheEnabled)),
    registerViewModel(new QStandardItemModel(Emulator::registerAmount, 2, this)),
    disasmViewModel(new QStandardItemModel(0, 3, this)),
    emuPending(true),
	updateVideoOutInterval(1000),
	updateUiDisplayedDataInterval(35),
    resetColorizingInterval(300),
	monitorSize(512, 512),
	registerDisplayBase(8),
	byteMaxDisplayLen(3),
    pdpNeedContinueRunning(false),
    pdpIsWaitingForInterrupt(false),
    fastModeEnabled(true)
{
	ui->setupUi(this);

	setTableViewConfiguration();
	setDisasmView();
	setHotkeys();
	setTimers();
	setFeatures();
    
    setFocusPolicy(Qt::StrongFocus);
    this->setFocus();
    
    emit updateVideoOut();
    emit updateUiDisplayedData();
    
    ui->showWordCheckBox->setChecked(true);
    emit on_showWordCheckBox_clicked(true);
    
    showState(GUI_STATE_INITIAL);
}

MainWindow::~MainWindow() {
	if (runnedCPUfuture.isRunning()) {
        emuPending.set(true);
		runnedCPUfuture.waitForFinished();
	}

	delete disasmViewModel;
	delete registerViewModel;
	delete pdpHandle;
	delete ui;
}

void MainWindow::on_stepiButton_clicked() {
    if (!emuPending.get()) return;
    
    QString instr = instrToRun();
    
    showState(GUI_STATE_RUNNING_INSTUCTION, instr);
	
    int err = cpuStepi();
    
    emit updateVideoOut();
	emit updateUiDisplayedData();
    scrollToInstrToRun();
    
    showState(err, instr);
}

void MainWindow::on_runButton_clicked() {
	if (emuPending.get()) {
		runnedCPUfuture = QtConcurrent::run(this, &MainWindow::cpuRun);
		futureWatcher.setFuture(runnedCPUfuture);
	} else {
        emuPending.set(true);
		runnedCPUfuture.waitForFinished();
	}
}


void MainWindow::on_displayBaseComboBox_currentIndexChanged(const QString &arg1) {
	registerDisplayBase = arg1.toInt();
	switch (registerDisplayBase) {
		case 2:
			byteMaxDisplayLen = 8;
			break;
		case 8:
		case 10:
			byteMaxDisplayLen = 3;
			break;
		case 16:
		default:
			byteMaxDisplayLen = 2;
			break;
	}
    emit updateUiDisplayedData();
}

void MainWindow::on_showWordCheckBox_clicked(bool checked) {
	setTableViewColumnLabels(checked);
    emit updateUiDisplayedData();
}

void MainWindow::on_disasmTableView_doubleClicked(const QModelIndex &index) {    
	int row = index.row();
	Qt::KeyboardModifiers modifiers = QApplication::queryKeyboardModifiers();

	if (modifiers.testFlag(Qt::ShiftModifier)) {
		QStringList instr = disasmViewModel->itemFromIndex(index)->text().split(" ");
        
		if (instr.contains("JMP") || instr.contains("JSR")) {
			int addr = instr.last().toInt(Q_NULLPTR, GUI_NUMBERS_BASE);
            unsigned newPos = addr/2;
            
			ui->disasmTableView->scrollTo(disasmViewModel->index(newPos, 1));
            makeHelpHighlightInDisasmTableView(newPos);
			
            return;
		}

		QStringList branchInstr;
		branchInstr << "BR" << "BNE" << "BEQ" << "BPL" << "BMI" << "BVC" <<
			    "BVS" << "BCC" << "BCS" << "BGE" << "BLT" << "BGT" <<
			    "BLE" << "BHI" << "BLOS" << "BHIS" << "BLO";

		if (branchInstr.contains(instr[0])) {
			const int diff = instr[1].toInt(Q_NULLPTR, GUI_NUMBERS_BASE);
			const int instr_size = 2;
            const int addr = (2*row + diff + instr_size)/2;
            
			ui->disasmTableView->scrollTo(disasmViewModel->index(addr, 1));
            makeHelpHighlightInDisasmTableView(addr);
		}
        return;        
	}
	
    breakpointMap[row] = !breakpointMap[row];
    emit updateUiDisplayedData();
}

void MainWindow::on_restartButton_clicked() {
    if (!emuPending.get()) return;

    pdpHandle->restart(ROM_dump_path);
    pdpHandle->restartPipeline(pipelineEnabled, cacheEnabled);

    pdpNeedContinueRunning = false;
    pdpIsWaitingForInterrupt = false;
    emit updateUiDisplayedData();
    emit updateVideoOut();
    scrollToInstrToRun();
    
    showState(GUI_STATE_INITIAL);
}

void MainWindow::on_pipelineComboBox_currentIndexChanged(int index)
{
    bool pipelineEnabled_new = (index == 0);

    if (pipelineEnabled_new != pipelineEnabled) {
        pipelineEnabled = pipelineEnabled_new;
        pdpHandle->restartPipeline(pipelineEnabled_new, cacheEnabled);

        showState(GUI_STATE_CHANGED_PIPELINE_CONFIG);
        emit updateUiDisplayedData();
    }
}

void MainWindow::on_fastModeComboBox_currentIndexChanged(int index)
{
    bool fastModeEnabled_old = fastModeEnabled;
    fastModeEnabled = (index == 0);

    if (fastModeEnabled != fastModeEnabled_old)
        showState(GUI_STATE_CHANGED_FASTMODE_CONFIG);
}


void MainWindow::on_cacheCheckBox_toggled(bool checked)
{
    cacheEnabled = checked;
    pdpHandle->restartPipeline(pipelineEnabled, cacheEnabled);

    showState(GUI_STATE_CHANGED_CACHE_CONFIG);
    emit updateUiDisplayedData();
}


void MainWindow::updateUiDisplayedData_called() {
    for (unsigned row = 0; row < Emulator::registerAmount; row++) {
		int16_t value = pdpHandle->Rn_get(row);

		if (ui->showWordCheckBox->isChecked()) {
			QModelIndex dataIndex = registerViewModel->index(row, 0, QModelIndex());
			QString data = QString("%1").arg(value & 0xFFFF,
							 2 * byteMaxDisplayLen, registerDisplayBase, QChar('0'));

			registerViewModel->setData(dataIndex, data);
		} else {
			QModelIndex highByteIndex = registerViewModel->index(row, 0, QModelIndex());
			QModelIndex lowByteIndex = registerViewModel->index(row, 1, QModelIndex());

			const int byteSize = 8;
			QString highByte = QString("%1").arg((value & 0xFF00) >> byteSize,
							     byteMaxDisplayLen, registerDisplayBase, QChar('0'));
			QString lowByte = QString("%1").arg(value & 0x00FF,
							    byteMaxDisplayLen, registerDisplayBase, QChar('0'));

			registerViewModel->setData(highByteIndex, highByte);
			registerViewModel->setData(lowByteIndex, lowByte);
		}
	}

	ui->fcEdit->setText(QString::number(pdpHandle->flag_get('C') != 0));
	ui->fvEdit->setText(QString::number(pdpHandle->flag_get('V') != 0));
	ui->fzEdit->setText(QString::number(pdpHandle->flag_get('Z') != 0));
	ui->fnEdit->setText(QString::number(pdpHandle->flag_get('N') != 0));
	ui->ftEdit->setText(QString::number(pdpHandle->flag_get('T') != 0));
	ui->fiEdit->setText(QString("%1").arg(pdpHandle->flag_get('I'), 11, 2, QChar('0')));

    foreach (int breakpointIndex, breakpointMap.keys()) {
        QModelIndex breakpointSignIndex = disasmViewModel->index(breakpointIndex, 0, QModelIndex());
		disasmViewModel->itemFromIndex(breakpointSignIndex)->setText(
			QString(breakpointMap[breakpointIndex] ? QChar(0x26AB) : 0));
		disasmViewModel->itemFromIndex(breakpointSignIndex)->setForeground(Qt::darkRed);
		disasmViewModel->itemFromIndex(breakpointSignIndex)->setTextAlignment(Qt::AlignCenter);
    }

    ui->pipelineStatusLabel->setText(QString("PDP11 Ticks %1").arg(pdpHandle->getClockNumber()));

    highlightLineInDisasmTableView(instrToRunN());   
}

void MainWindow::resetColorizing_called()
{
    while (!disasmViewColorizingList.isEmpty()) {
        QPair<unsigned, QTime> node = disasmViewColorizingList.first();
        const int delta = 250;
        
        if (node.second.msecsTo(QTime::currentTime()) < delta)
            return;
        
        QModelIndex index = ui->disasmTableView->model()->index(node.first, 2);    
        disasmViewModel->itemFromIndex(index)->setBackground(Qt::white);
        
        disasmViewColorizingList.removeFirst();
    }
    
    resetColorizingTimer.stop();
}

void MainWindow::runnedCPUfuture_stopped()
{
    updateUiDisplayedDataTimer.stop();
    updateVideoOutTimer.stop();   
    ui->stepiButton->setEnabled(true);
	ui->restartButton->setEnabled(true);
    ui->pipelineComboBox->setEnabled(true);
    ui->fastModeComboBox->setEnabled(true);
    
    emit updateUiDisplayedData();
    emit updateVideoOut();
    scrollToInstrToRun();
    
    showState(futureWatcher.result());
}

void MainWindow::runnedCPUfuture_started()
{
    ui->stepiButton->setEnabled(false);
	ui->restartButton->setEnabled(false);
    ui->pipelineComboBox->setEnabled(false);
    ui->fastModeComboBox->setEnabled(false);
    
    updateUiDisplayedDataTimer.start(updateUiDisplayedDataInterval);
    updateVideoOutTimer.start(updateVideoOutInterval);
    
    showState(GUI_STATE_RUNNING);
}

void MainWindow::updateVideoOut_called() {
	QImage img(pdpHandle->VRAM_get(),
		   pdpHandle->display.width, pdpHandle->display.height, QImage::Format_Grayscale8);
	ui->videoOut->setPixmap(QPixmap::fromImage(img.scaled(monitorSize, Qt::KeepAspectRatio)));
}

void MainWindow::setTableViewConfiguration() {
	setTableViewColumnLabels(false);

	QStringList rowHeaders;
    for (int row = 0; row < Emulator::registerAmount; row++) {
		rowHeaders << QString("R%1").arg(row);
	}
	registerViewModel->setVerticalHeaderLabels(rowHeaders);

	ui->rTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui->rTableView->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui->rTableView->setModel(registerViewModel);
}

void MainWindow::setTableViewColumnLabels(bool wordMode) {
	QStringList columnHeaders;

	if (wordMode) {
		columnHeaders << "Word";
		ui->rTableView->setColumnHidden(1, true);
	} else {
		columnHeaders << "HighByte" << "LowByte";
		ui->rTableView->setColumnHidden(1, false);
	}

	registerViewModel->setHorizontalHeaderLabels(columnHeaders);
}

void MainWindow::setHotkeys() {
	ui->stepiButton->setShortcut(QKeySequence(Qt::Key_F11));
	ui->runButton->setShortcut(QKeySequence(Qt::Key_F5));
	ui->restartButton->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_R));
	ui->showWordCheckBox->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_W));
}

void MainWindow::setDisasmView() {
	QStringList disasmList;
    pdpHandle->makeDisasm(disasmList);
	int haltComboLenght = 0;
            
	for (int i = 0; i < disasmList.length(); i++) {
        QString instr = disasmList[i];
        
		if (instr.split(" ")[0].contains("HALT")) {
            if (++haltComboLenght > 2)
                hiddenWordsInDisasmView[i] = true;
		} else {
            if (haltComboLenght > 2)
                hiddenWordsInDisasmView[i-1] = false;
            
			haltComboLenght = 0;
		}
        
#ifndef DONT_HIDE_DATA_WORDS
        if (instr.contains(DATA_WORD_PLACEHOLDER)) {
            hiddenWordsInDisasmView[i] = true;
        }
#endif

		QStandardItem *Item = new QStandardItem();
        Item->setTextAlignment(Qt::AlignCenter);
        Item->setText(instr);
        disasmViewModel->setItem(i, 2, Item);  
        
		QModelIndex index = disasmViewModel->index(i, 1);
        QString offsetStr = snum_QString(i*2);
        
		disasmViewModel->itemFromIndex(index)->setText(offsetStr);
        disasmViewModel->itemFromIndex(index)->setTextAlignment(Qt::AlignCenter);
	}

	ui->disasmTableView->setModel(disasmViewModel);
    
    foreach (int rowToHide, hiddenWordsInDisasmView.keys()) {
        if (hiddenWordsInDisasmView[rowToHide])
             ui->disasmTableView->hideRow(rowToHide);
    }
    
	ui->disasmTableView->setColumnWidth(0, 35);
	ui->disasmTableView->setColumnWidth(1, 70);
    ui->disasmTableView->verticalHeader()->hide();
	ui->disasmTableView->horizontalHeader()->setStretchLastSection(true);

	QStringList horisontalHeader;
	horisontalHeader << "B" << "0ffset" << "Instruction";
    disasmViewModel->setHorizontalHeaderLabels(horisontalHeader);
}

void MainWindow::setTimers() {
	QObject::connect(&updateVideoOutTimer, SIGNAL(timeout()),
			 this, SLOT(updateVideoOut_called()));
    
	QObject::connect(&updateUiDisplayedDataTimer, SIGNAL(timeout()),
			 this, SLOT(updateUiDisplayedData_called()));
    
    QObject::connect(&resetColorizingTimer, SIGNAL(timeout()),
			 this, SLOT(resetColorizing_called()));
}

void MainWindow::setFeatures() {
	QObject::connect(&futureWatcher, SIGNAL(finished()), 
                     this, SLOT(runnedCPUfuture_stopped()));
    QObject::connect(&futureWatcher, SIGNAL(started()), 
                     this, SLOT(runnedCPUfuture_started()));
    
    QObject::connect(this, SIGNAL(updateVideoOut()), 
                     this, SLOT(updateVideoOut_called()));
    QObject::connect(this, SIGNAL(updateUiDisplayedData()), 
                     this, SLOT(updateUiDisplayedData_called()));
}

void SharedBool::set(bool newState)
{
    QWriteLocker lock(&flagMutex);
    flag = newState;
}

bool SharedBool::get()
{
    QReadLocker lock(&flagMutex);
    return flag;
}


void MainWindow::highlightLineInDisasmTableView(unsigned line) {
	QModelIndex index = ui->disasmTableView->model()->index(line, 0);
    ui->disasmTableView->setRowHidden(line, false);
	ui->disasmTableView->selectionModel()->select(index,
						      QItemSelectionModel::ClearAndSelect
						      | QItemSelectionModel::Rows);
}

void MainWindow::makeHelpHighlightInDisasmTableView(unsigned line)
{
    QModelIndex index = ui->disasmTableView->model()->index(line, 2);    
    disasmViewModel->itemFromIndex(index)->setBackground(Qt::yellow);
    QPair<unsigned, QTime> newItem(line, QTime::currentTime());
    disasmViewColorizingList.push_front(newItem);
    
    if (!resetColorizingTimer.isActive())
        resetColorizingTimer.start(resetColorizingInterval);
}

int MainWindow::cpuRun() {
    emuPending.set(false);
    
	int err;
	do {
        err = cpuStepi();
	} while (!emuPending.get() && !err && !breakpointMap.value(instrToRunN(), false));   
    
    pdpNeedContinueRunning = (err == Emulator::Status::Wait);
        
    emuPending.set(true);
    return err;
}


int MainWindow::cpuStepi() {
    uint64_t oldTime = pdpHandle->getClockNumber();

    int res = pdpHandle->stepi();

    if (!fastModeEnabled)
        QThread::msleep(PDP11_TIME_PER_TICK*(pdpHandle->getClockNumber() - oldTime));

    pdpIsWaitingForInterrupt = (res == Emulator::Status::Wait);
    pdpNeedContinueRunning = false;

    return res;
}

uint16_t MainWindow::instrToRunN() {
    return (uint16_t) pdpHandle->Rn_get(Emulator::registerAmount - 1) >> 1;
}

QString MainWindow::instrToRun()
{
    return disasmViewModel->item(instrToRunN(), 2)->text();
}

void MainWindow::feedKeyBuffer(int scancode)
{
    pdpHandle->feedCharQueue(scancode);

    if (pdpIsWaitingForInterrupt) {
        pdpIsWaitingForInterrupt = false;

        if (pdpNeedContinueRunning) {
            pdpNeedContinueRunning = false;
            ui->runButton->animateClick();
        } else
            ui->stepiButton->animateClick();
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    feedKeyBuffer(event->nativeScanCode());
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() != Qt::Key_Shift)
        return;

    feedKeyBuffer(event->nativeScanCode());
}


QString MainWindow::instrRunResultStr(int res)
{
    int stateToDisplay;
    using namespace Emulator::Status;
    
    switch (res) {
    case Failed:
        stateToDisplay = GUI_STATE_LAST_INSTRUCTION_FAILED;
        break;
    case Success:
        stateToDisplay = GUI_STATE_FINISHED_INSTRUCTION;
        break;
    case Halt:
        stateToDisplay = GUI_STATE_RUN_FINISHED;
        break;
    case Wait:
        stateToDisplay = GUI_STATE_WAIT;
        break;
    default:
        assert(!"BUG");
        stateToDisplay = GUI_STATE_NO_MSG;
        break;
    }
    
    return pdp11StateMsgs[stateToDisplay];
}

void MainWindow::showState(pdp11_gui_states state, QString &info) {
    ui->statusBar->showMessage(pdp11StateMsgs[state] + " (" + info + ")");
}

void MainWindow::showState(pdp11_gui_states state) {
    ui->statusBar->showMessage(pdp11StateMsgs[state]);
}

void MainWindow::showState(QString &info) {
    ui->statusBar->showMessage(info);
}


void MainWindow::showState(int res, QString &info) {
    ui->statusBar->showMessage(instrRunResultStr(res) + " (" + info + ")");
}

void MainWindow::showState(int res) {
    ui->statusBar->showMessage(instrRunResultStr(res));
}


void MainWindow::scrollToInstrToRun()
{
    ui->disasmTableView->scrollTo(disasmViewModel->index(instrToRunN(), 1));
}


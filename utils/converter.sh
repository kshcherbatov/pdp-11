#!/bin/bash

if [ $# -ne 3 ]; then
    echo $0: usage: converter boot-code inthandle-code output-file
    exit 1
fi

macro11/macro11 $1 -o /dev/stdout | obj2hex/obj2hex.pl --ascii --objfile=/dev/stdin --outfile=/dev/stdout \
	| grep D | awk '{print $2}' | ./octBE_to_hexLE.py | xxd -r -p > $3

macro11/macro11 $2 -o /dev/stdout | obj2hex/obj2hex.pl --ascii --objfile=/dev/stdin --outfile=/dev/stdout \
	| grep D | awk '{print $2}' | ./octBE_to_hexLE.py | xxd -r -p > _inthandle.tmp 

convert logo.png -resize 40x40\!  -type palette -depth 2 -colorspace gray _logo.tmp
convert font.png -type palette -depth 2 -colorspace gray _font.tmp

./bitmap.py $3 
rm _logo.tmp
rm _font.tmp
rm _inthandle.tmp

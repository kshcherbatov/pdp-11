#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QStandardItemModel>
#include <QMap>
#include <QReadWriteLock>
#include <QTime>
#include <QList>
#include <QPair>
#include <QtConcurrent/QtConcurrent>
#include <QKeyEvent>
#include "model/PDP11.h"

namespace Ui {
class MainWindow;
}

enum pdp11_gui_states {
    GUI_STATE_FINISHED_INSTRUCTION = Emulator::Status::Success,
    GUI_STATE_WAIT = Emulator::Status::Wait,
    GUI_STATE_HALT = Emulator::Status::Halt,
    GUI_STATE_LAST_INSTRUCTION_FAILED = Emulator::Status::Failed,
    GUI_STATE_INITIAL = 100,
    GUI_STATE_RUNNING = 101,
    GUI_STATE_RUN_FINISHED = 102,
    GUI_STATE_RUNNING_INSTUCTION = 103,
    GUI_STATE_CHANGED_PIPELINE_CONFIG = 103,
    GUI_STATE_CHANGED_FASTMODE_CONFIG = 104,
    GUI_STATE_CHANGED_CACHE_CONFIG = 105,
    GUI_STATE_NO_MSG = 106
};

class SharedBool {
public:
    void set(bool newState);
    bool get();
    SharedBool(): flag(false){};
    SharedBool(bool _value): flag(_value){};
private:
    QReadWriteLock flagMutex;
	bool flag;
};


class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
    
public slots:
	void updateVideoOut_called();
	void updateUiDisplayedData_called();
    void resetColorizing_called();
    void runnedCPUfuture_stopped();
    void runnedCPUfuture_started();

private slots:
	void on_stepiButton_clicked();
	void on_displayBaseComboBox_currentIndexChanged(const QString &arg1);
	void on_showWordCheckBox_clicked(bool checked);

	void on_disasmTableView_doubleClicked(const QModelIndex &index);
	void on_runButton_clicked();
	void on_restartButton_clicked();
    
    void on_pipelineComboBox_currentIndexChanged(int index);
    void on_fastModeComboBox_currentIndexChanged(int index);
    void on_cacheCheckBox_toggled(bool checked);

signals:
	void updateVideoOut();
	void updateUiDisplayedData();
    void characterGenerated(QChar character);

private:
	void setTableViewConfiguration();
	void setTableViewColumnLabels(bool wordMode);
	void setHotkeys();
	void setTimers();
	void setDisasmView();

    void highlightLineInDisasmTableView(unsigned line);
    void makeHelpHighlightInDisasmTableView(unsigned line);
    void scrollToInstrToRun();
    void showState(int res);
    void showState(int res, QString &info);
    void showState(pdp11_gui_states state);
    void showState(pdp11_gui_states state, QString &info);
    void showState(QString &info);
    void feedKeyBuffer(int scancode);
    
    QString instrRunResultStr(int res);
    
	int cpuStepi();
	uint16_t instrToRunN();
    QString instrToRun();

	void setFeatures();
	int cpuRun();

	Ui::MainWindow *ui;

    bool pipelineEnabled;
    bool cacheEnabled;

    Emulator::PDP11 *pdpHandle;
	QStandardItemModel *registerViewModel;
    QStandardItemModel *disasmViewModel;
    
    SharedBool emuPending;
    
    const unsigned updateVideoOutInterval;
	const unsigned updateUiDisplayedDataInterval;
    const unsigned resetColorizingInterval;
    const QSize monitorSize;
    int registerDisplayBase;
	int byteMaxDisplayLen;
    
	QTimer updateVideoOutTimer;
	QTimer updateUiDisplayedDataTimer;
    QTimer resetColorizingTimer;
    
	QMap<int, bool> breakpointMap;
    QMap<int, bool> hiddenWordsInDisasmView;
    
    QList< QPair<unsigned, QTime> > disasmViewColorizingList;
    
	QFuture<int> runnedCPUfuture;
	QFutureWatcher<int> futureWatcher;
    
    bool pdpNeedContinueRunning;
    bool pdpIsWaitingForInterrupt;

    bool fastModeEnabled;

protected:
   virtual void keyPressEvent(QKeyEvent *event);
   virtual void keyReleaseEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H

		MOV #40004,  R1 ; THIRD RAM WORD - SHIFT MODIFICATOR
		MOV #0, R2 
		MOV R2, (R1)
	
		MOV #137770, R1	   ; VRAM address
		MOV #2000,   R2	   ; ROM data address
		MOV #620,    R3    ; data amount
		MOV #12,     R4    ; row size
BEGIN:	MOVB (R2)+,  (R1)+
		DEC R4
		BEQ NEWROW
ITER:	DEC R3
		BNE BEGIN
		; SAVE PLACE TO VOUT
		
		MOV #HSTR, 	R4
		JSR	PC,     PRINT
		
		MOV #40002, R3 ; SECOND RAM WORD - VOUT PLACE
		MOV R1, (R3)
		
INPUT:  WAIT
	    BR INPUT
		HALT
	
NEWROW:	ADD #66,     R1
		MOV #12,     R4
		JMP ITER
		
; ######## LETER(R2)  --- draw leter  ###############
; R2 --- Letter number to draw
; R1 --- VRAM address to start drawing
; letter size is 0500=320=256+64=2^8+2^6
LETER:	MOV R1,    -(SP)
		MOV R3,    -(SP) ; to use R3 later
		MOV R2,     R3
		ASH #6, 	R2
		ASH #4, 	R3
		ADD R3,		R2
		ADD #2620,  R2
		MOV #20,    R3
		JSR PC,     SYMBOL
		MOV (SP)+,  R3
		MOV (SP)+,  R1
		ADD #5,     R1
		; NEW LINE NEEDED?
		MOV R4, 	-(SP)
		MOV R3,		-(SP)
		MOV #40000,  R3  ; FIRST RAM WORD CONTAINS SYMBOL ON THE LINE AMOUNT
		MOV (R3), 	R4
		ADD #1,		R4
		MOV R4, 	(R3)
		SUB #14,    R4
		BMI LTREND
		MOV #0, 	(R3)
		ADD #2004,  R1
LTREND:	MOV (SP)+,	R3
		MOV (SP)+,	R4
		RTS PC
		
; ##### PRINT --- print string ###########
; R4 --- STR address
; R1 --- VRAM address to start drawing
PRINT:  MOV R2, 	-(SP)
PRBGN:	MOV (R4), 	R2
		BEQ PREND
		ADD #2, 	R4
		JSR PC,     LETER
		BR PRBGN
PREND:	MOV (SP)+, R2
		RTS PC

; ######## SYMBOL --- draw symbol ###############
; R1 --- VRAM address to start drawing
; R2 --- ROM data address
; R3 --- number of rows to draw
SYMBOL:	MOV R1,    -(SP)
		MOV R3,    -(SP)
		MOV #5,      R3
		JSR PC,      SYMBRO
		MOV (SP)+,   R3
		MOV (SP)+,   R1
		ADD #100,    R1
		DEC R3
		BNE SYMBOL
		RTS PC

; ######## SYMBRO --- draw symbol row ############
; R1 --- VRAM address to start drawing
; R2 --- ROM data address
; R3 --- number of columns to draw
SYMBRO: MOVB (R2)+,  (R1)+
		DEC R3
		BNE SYMBRO
		RTS PC
		
; &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
; &&&&&&&&             DATA SECTION              &&&&&&&&
; &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
HSTR:	51,35,51,65,65, 77	; PDP11:
		0 ; END OF STRING
; &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
; &&&&&&&&     \\\      &&&&&&&&&      ///       &&&&&&&&
; &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

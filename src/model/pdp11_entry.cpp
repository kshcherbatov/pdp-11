#include "pdp11_entry.h"
#include "pdp11_unibus.h"
#include "instruction_set/instr_funcs.h"

extern unsigned char *_MEMORY_BEGIN;

template<>
uint16_t pdp11_word_t::get() {
	return __le16_to_cpu(__entry);
}

template<>
uint8_t pdp11_byte_t::get() {
	return __entry;
}

template<>
void pdp11_word_t::set(uint16_t value) {
    if( _MEMORY_BEGIN != 0 && (unsigned char *)this < _MEMORY_BEGIN + Emulator::ROM_size)
        throw INSTR_CODE_INVALID_ADDRESS;
	__entry = __cpu_to_le16(value);
}

template<>
void pdp11_byte_t::set(uint8_t value) {
    if( _MEMORY_BEGIN != 0 && (unsigned char *)this < _MEMORY_BEGIN + Emulator::ROM_size)
        throw INSTR_CODE_INVALID_ADDRESS;
	__entry = value;
}

#define typename_T typename pdp_t, typename unsigned_cpu_t, typename signed_cpu_t
#define __T pdp_t, unsigned_cpu_t, signed_cpu_t

template<typename_T>
pdp11_entry<__T> pdp11_entry<__T>::rawed()
{
    pdp11_entry ret;
    signed_cpu_t raw = get();
    ret.set((raw < 0) ? -raw : raw);
    return ret;
}

template<>
uint8_t pdp11_word_t::high_bit() {
	return this->get() >> 15;
}

template<>
uint8_t pdp11_byte_t::high_bit() {
	return this->get() >> 7;
}

template<typename_T>
uint8_t pdp11_entry<__T>::low_bit() {
	return this->get() & 1;
}

template<typename_T>
uint8_t pdp11_entry<__T>::low_byte() {
	return this->get() & 0xff;
}

template<>
pdp11_word_t pdp11_word_t::rshifted(unsigned op) {
    return { __cpu_to_le16(this->get() >> op)};
}

template<>
pdp11_byte_t pdp11_byte_t::rshifted(unsigned op) {
    return { (__u8)(this->get() >> op) };
}

template<>
pdp11_word_t pdp11_word_t::lshifted(unsigned op) {
    return { __cpu_to_le16(this->get() << op)};
}

template<>
pdp11_byte_t pdp11_byte_t::lshifted(unsigned op) {
    return { (__u8)(this->get() << op) };
}

template<typename_T>
void pdp11_entry<__T>::inc() {
	this->set(this->get() + 1);
}

template<typename_T>
void pdp11_entry<__T>::inc2() {
	this->set(this->get() + 2);
}

template<typename_T>
void pdp11_entry<__T>::dec() {
	this->set(this->get() - 1);
}

template<typename_T>
void pdp11_entry<__T>::dec2() {
	this->set(this->get() - 2);
}

template<typename_T>
void pdp11_entry<__T>::add(pdp11_entry<__T> summand) {
	this->set(this->get() + (signed_cpu_t)summand.get());
}

template<typename_T>
void pdp11_entry<__T>::add(signed_cpu_t summand) {
	this->set(this->get() + summand);
}

template<typename_T>
void pdp11_entry<__T>::sub(pdp11_entry<__T> subtrahend) {
	this->set(this->get() - (signed_cpu_t)subtrahend.get());
}

template<typename_T>
void pdp11_entry<__T>::sub(signed_cpu_t subtrahend) {
	this->set(this->get() - subtrahend);
}

template<typename_T>
uint8_t pdp11_entry<__T>::is_zero() {
	return (this->get() == 0);
}

template<typename_T>
uint8_t pdp11_entry<__T>::is_lower_zero() {
	return ((signed_cpu_t)this->get() < 0);
}

static const uint16_t Word_head = 1 << 15;
static const uint8_t  Byte_head = 1 << 7;
static const uint16_t Signed_word_max = Word_head - 1;
static const int8_t   Signed_byte_max = Byte_head - 1;
static const uint16_t UnSigned_word_max = (1 << 16) - 1;
static const uint8_t  UnSigned_byte_max = (1 << 8) - 1;

template<>
uint8_t pdp11_word_t::is_head() {
	return this->get() == Word_head;
}

template<>
uint8_t pdp11_byte_t::is_head() {
	return this->get() == Byte_head;
}

template<>
uint8_t pdp11_word_t::is_signed_max() {
	return this->get() == Signed_word_max;
}

template<>
uint8_t pdp11_byte_t::is_signed_max() {
	return this->get() == Signed_byte_max;
}

template<>
uint8_t pdp11_word_t::is_unsigned_max() {
	return this->get() == UnSigned_word_max;
}

template<>
uint8_t pdp11_byte_t::is_unsigned_max() {
	return this->get() == UnSigned_byte_max;
}

/* Linker stuff */

template uint16_t pdp11_word_t::get();
template uint8_t  pdp11_byte_t::get();
template void pdp11_word_t::set(uint16_t entry);
template void pdp11_byte_t::set(uint8_t  entry);

template void pdp11_word_t::inc();
template void pdp11_byte_t::inc();
template void pdp11_word_t::dec();
template void pdp11_byte_t::dec();
template void pdp11_word_t::inc2();
template void pdp11_byte_t::inc2();
template void pdp11_word_t::dec2();
template void pdp11_byte_t::dec2();
template void pdp11_word_t::add(int16_t summand);
template void pdp11_byte_t::add(int8_t  summand);
template void pdp11_word_t::add(pdp11_word_t summand);
template void pdp11_byte_t::add(pdp11_byte_t summand);
template void pdp11_word_t::sub(int16_t subtrahend);
template void pdp11_byte_t::sub(int8_t  subtrahend);
template void pdp11_word_t::sub(pdp11_entry subtrahend);
template void pdp11_byte_t::sub(pdp11_entry subtrahend);

template uint8_t pdp11_word_t::high_bit();	
template uint8_t pdp11_byte_t::high_bit();	
template uint8_t pdp11_word_t::low_bit();
template uint8_t pdp11_byte_t::low_bit();
template uint8_t pdp11_word_t::low_byte();
template uint8_t pdp11_byte_t::low_byte();
    
template pdp11_word_t pdp11_word_t::rawed();
template pdp11_byte_t pdp11_byte_t::rawed();
template pdp11_word_t pdp11_word_t::rshifted(unsigned op);
template pdp11_byte_t pdp11_byte_t::rshifted(unsigned op);
template pdp11_word_t pdp11_word_t::lshifted(unsigned op);
template pdp11_byte_t pdp11_byte_t::lshifted(unsigned op);
    
template uint8_t pdp11_word_t::is_zero();
template uint8_t pdp11_byte_t::is_zero();
template uint8_t pdp11_word_t::is_lower_zero();
template uint8_t pdp11_byte_t::is_lower_zero();
template uint8_t pdp11_word_t::is_head();
template uint8_t pdp11_byte_t::is_head();
template uint8_t pdp11_word_t::is_signed_max();
template uint8_t pdp11_byte_t::is_signed_max();
template uint8_t pdp11_word_t::is_unsigned_max();
template uint8_t pdp11_byte_t::is_unsigned_max();

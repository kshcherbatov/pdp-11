#!/usr/bin/python3

import sys

for line in sys.stdin:
	word = int(line, base=8);
	print("%02x" % (word & 0xff));
	print("%02x" % (word >> 8));

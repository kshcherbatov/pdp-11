#include "pdp11_pipeline.h"

void InstructionCtx::addLoadByteCtx(addr_mode_t mode, void *address) {
    assert(loadNumber < InstructionDescription::maxMemoryAccessNumber);
    loadCtx[loadNumber].mode = mode;
    loadCtx[loadNumber].address = address;
    loadNumber++;
}

void InstructionCtx::addLoadWordCtx(addr_mode_t mode, void *address) {
    this->addLoadByteCtx(mode, address);
    this->addLoadByteCtx(mode, (char *)address + 1);
}

void InstructionCtx::addStoreByteCtx(addr_mode_t mode, void *address) {
    assert(storeNumber < InstructionDescription::maxMemoryAccessNumber);
    storeCtx[storeNumber].mode = mode;
    storeCtx[storeNumber].address = address;
    storeNumber++;
}

void InstructionCtx::addStoreWordCtx(addr_mode_t mode, void *address) {
    this->addStoreByteCtx(mode, address);
    this->addStoreByteCtx(mode, (char *)address + 1);
}

void pdp11_pipeline::evaluate(const InstructionCtx &ctx) {
    currentInstructionDescription.f_count = fetchTime;
    currentInstructionDescription.d_count = decodeTime;

    currentInstructionDescription.l_count = 0;
    for(uintptr_t i = 0; i < ctx.loadNumber; i++) {
        if( cache_enabled && cache.contain(ctx.loadCtx[i].address) )
            currentInstructionDescription.l_count += loadCacheTime[ctx.loadCtx[i].mode];
        else
            currentInstructionDescription.l_count += loadTime[ctx.loadCtx[i].mode];
        if( cache_enabled )
            cache.insert(ctx.loadCtx[i].address);
    }

    currentInstructionDescription.e_count = execTime[ctx.operation];

    currentInstructionDescription.s_count = 0;
    for(uintptr_t i = 0; i < ctx.storeNumber; i++) {
        if( cache_enabled && cache.contain(ctx.storeCtx[i].address) )
            currentInstructionDescription.s_count += storeCacheTime[ctx.storeCtx[i].mode];
        else
            currentInstructionDescription.s_count += storeTime[ctx.storeCtx[i].mode];
        if( cache_enabled )
            cache.insert(ctx.storeCtx[i].address);
    }

    if( enabled ) {
        currentInstructionDescription.loadNumber = ctx.loadNumber;
        currentInstructionDescription.storeNumber = ctx.storeNumber;
        for(uintptr_t i = 0; i < ctx.loadNumber; i++)
            currentInstructionDescription.loadAddresses[i] = ctx.loadCtx[i].address;
        for(uintptr_t i = 0; i < ctx.storeNumber; i++)
            currentInstructionDescription.storeAddresses[i] = ctx.storeCtx[i].address;
        table.addRow(currentInstructionDescription);
    }
    else totalTime += currentInstructionDescription.f_count + currentInstructionDescription.d_count + \
                      currentInstructionDescription.l_count + currentInstructionDescription.e_count + \
            currentInstructionDescription.s_count;
}

void pdp11_pipeline::reset()
{
    if (enabled)
        table.reset();
}


void pdp11_pipeline::restart(bool is_enabled, bool cache_is_enabled)
{
    enabled = is_enabled;
    cache_enabled = cache_is_enabled;
    restart();
}

void pdp11_pipeline::restart()
{
    table.restart();
    cache.restart();
    totalTime = 0;
}

pdp11_pipeline::pdp11_pipeline(bool is_enabled, bool cache_is_enabled):
    enabled(is_enabled),
    totalTime(0),
    fetchTime(1),
    decodeTime(1),
    cache_enabled(cache_is_enabled),
    cache(8)
{
    using namespace Emulator;
    loadTime[addressingModes::Register] = 1;
    loadTime[addressingModes::RegisterDeferred] = 40;
    loadTime[addressingModes::Autoincrement] = 41;
    loadTime[addressingModes::Autodecrement] = 41;
    loadTime[addressingModes::AutoincrementDeferred] = 82;
    loadTime[addressingModes::AutodecrementDeferred] = 82;
    loadTime[addressingModes::Index] = 82;
    loadTime[addressingModes::IndexDeferred] = 124;

    loadCacheTime[addressingModes::Register] = 1;
    loadCacheTime[addressingModes::RegisterDeferred] = 5;
    loadCacheTime[addressingModes::Autoincrement] = 6;
    loadCacheTime[addressingModes::Autodecrement] = 6;
    loadCacheTime[addressingModes::AutoincrementDeferred] = 11;
    loadCacheTime[addressingModes::AutodecrementDeferred] = 11;
    loadCacheTime[addressingModes::Index] = 11;
    loadCacheTime[addressingModes::IndexDeferred] = 18;

    storeTime[addressingModes::Register] = 1;
    storeTime[addressingModes::RegisterDeferred] = 40;
    storeTime[addressingModes::Autoincrement] = 41;
    storeTime[addressingModes::Autodecrement] = 41;
    storeTime[addressingModes::AutoincrementDeferred] = 82;
    storeTime[addressingModes::AutodecrementDeferred] = 82;
    storeTime[addressingModes::Index] = 82;
    storeTime[addressingModes::IndexDeferred] = 124;

    storeCacheTime[addressingModes::Register] = 1;
    storeCacheTime[addressingModes::RegisterDeferred] =5;
    storeCacheTime[addressingModes::Autoincrement] = 6;
    storeCacheTime[addressingModes::Autodecrement] = 6;
    storeCacheTime[addressingModes::AutoincrementDeferred] = 11;
    storeCacheTime[addressingModes::AutodecrementDeferred] = 11;
    storeCacheTime[addressingModes::Index] = 11;
    storeCacheTime[addressingModes::IndexDeferred] = 18;

    uint64_t microcodeOperationMultiplier = 2;

#define EXEC_TIME_INIT(mnem, micro_op_num)\
    execTime[instructionMnemonics::mnem] = micro_op_num*microcodeOperationMultiplier

    EXEC_TIME_INIT(CLR,  1);
    EXEC_TIME_INIT(COM,  1);
    EXEC_TIME_INIT(INC,  1);
    EXEC_TIME_INIT(DEC,  1);
    EXEC_TIME_INIT(NEG,  1);
    EXEC_TIME_INIT(TST,  1);
    EXEC_TIME_INIT(ASR,  1);
    EXEC_TIME_INIT(ASL,  1);
    EXEC_TIME_INIT(ROR,  1);
    EXEC_TIME_INIT(ROL,  1);
    EXEC_TIME_INIT(SWAB, 1);
    EXEC_TIME_INIT(ADC,  1);
    EXEC_TIME_INIT(SBC,  1);
    EXEC_TIME_INIT(SXT,  1);
    EXEC_TIME_INIT(MOV,  1);
    EXEC_TIME_INIT(CMP,  1);
    EXEC_TIME_INIT(ADD,  1);
    EXEC_TIME_INIT(BIT,  1);
    EXEC_TIME_INIT(BIC,  1);
    EXEC_TIME_INIT(BIS,  1);
    EXEC_TIME_INIT(ASH,  2);
    EXEC_TIME_INIT(XOR,  2);
    EXEC_TIME_INIT(BR,   1);
    EXEC_TIME_INIT(BNE,  1);
    EXEC_TIME_INIT(BEQ,  1);
    EXEC_TIME_INIT(BPL,  1);
    EXEC_TIME_INIT(BPL,  1);
    EXEC_TIME_INIT(BMI,  1);
    EXEC_TIME_INIT(BVC,  1);
    EXEC_TIME_INIT(BVS,  1);
    EXEC_TIME_INIT(BCC,  1);
    EXEC_TIME_INIT(BCS,  1);
    EXEC_TIME_INIT(BGE,  1);
    EXEC_TIME_INIT(BLT,  1);
    EXEC_TIME_INIT(BGT,  1);
    EXEC_TIME_INIT(BLE,  1);
    EXEC_TIME_INIT(BHI,  1);
    EXEC_TIME_INIT(BLOS, 1);
    EXEC_TIME_INIT(BHIS, 1);
    EXEC_TIME_INIT(BLO,  1);
    EXEC_TIME_INIT(JMP,  1);
    EXEC_TIME_INIT(JSR,  4);
    EXEC_TIME_INIT(RTS,  2);
    EXEC_TIME_INIT(MARK, 3);
    EXEC_TIME_INIT(RTI,  2);
    EXEC_TIME_INIT(RTT,  2);
    EXEC_TIME_INIT(HALT, 8);
    EXEC_TIME_INIT(WAIT, 4);

#undef EXEC_TIME_INIT

}

#include "LRU_cache.h"
#include <cassert>

bool LRU_cache::contain(void *addr) {
    return set.count(addr) > 0;
}

void LRU_cache::restart()
{
    set.clear();
    list.clear();
}

void LRU_cache::insert(void *addr) {
	if(list.size() >= capacity) {
		set.erase(list.back());	
		list.pop_back();
	}
	if(this->contain(addr)) {
		list.remove(addr);
	}
	list.push_front(addr);
	set.insert(addr);
}

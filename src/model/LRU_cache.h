#ifndef __LRU_CACHE_H
#define __LRU_CACHE_H

#include <cstdint>
#include <list>
#include <unordered_set>
#include <cassert>

class LRU_cache {
	uint32_t capacity;
    std::list<void *> list;
    std::unordered_set<void *> set;
public:
	LRU_cache(uint32_t _capacity): capacity(_capacity) {
		assert(capacity > 0);
	}
	void insert(void *addr);
	bool contain(void *addr);
    void restart();
};

#endif

pixel_map = {
	0b00 : "00",
	0b01 : "0f",
	0b10 : "f0",
	0b11 : "ff"
};

def print_delim(x):
	if(x == 0xff):
		print("\\\n};\n");
	elif(x % 8 != 7):
		print(", ", end="");
	else:
		print(",\\\n\t", end="");

print("#include <endian.h>")
print("#if __BYTE_ORDER == __LITTLE_ENDIAN");
print("#define PIXELMAP_INITIALIZER {\\\n\t", end="");
for x in range(0x100):
	print("0x", end="");
	print(pixel_map[ x & 0b00000011],       end="");
	print(pixel_map[(x & 0b00001100) >> 2], end="");
	print(pixel_map[(x & 0b00110000) >> 4], end="");
	print(pixel_map[(x & 0b11000000) >> 6], end="");
	print_delim(x);

print("#elif __BYTE_ORDER == __BIG_ENDIAN");
print("#define PIXELMAP_INITIALIZER {\\\n\t", end="");
for x in range(0x100):
	print("0x", end="");
	print(pixel_map[(x & 0b11000000) >> 6], end="");
	print(pixel_map[(x & 0b00110000) >> 4], end="");
	print(pixel_map[(x & 0b00001100) >> 2], end="");
	print(pixel_map[ x & 0b00000011],       end="");
	print_delim(x);

print("#elif __BYTE_ORDER == __PDP_ENDIAN");
print("#define PIXELMAP_INITIALIZER {\\\n\t", end="");
for x in range(0x100):
	print("0x", end="");
	print(pixel_map[(x & 0b00110000) >> 4], end="");
	print(pixel_map[(x & 0b11000000) >> 6], end="");
	print(pixel_map[ x & 0b00000011],       end="");
	print(pixel_map[(x & 0b00001100) >> 2], end="");
	print_delim(x);

print("#else");
print("#error \"no byte order defined\"");
print("#endif\n");

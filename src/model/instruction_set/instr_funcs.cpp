#include <assert.h>
#include <functional>
#include "instr_set_init.h"
#include "instr_funcs.h"
#include "../pdp11_unibus.h"
#include "../pdp11_pipeline.h"

/* N.B.: Be careful: the order of the operations is make sense! */

#define IS_ZERO(ARG) \
	(ARG == 0)

#define IS_LOWER_ZERO(ARG) \
	(ARG < 0)

#define GET_LOW_BIT(ARG) \
	(ARG & 1)

#define GET_HIGH_BIT_WORD(ARG) \
	((uint16_t)ARG >> 15)

#define GET_HIGH_BIT_BYTE(ARG) \
	((__u8)ARG >> 7)

#define GET_LOW_BYTE(ARG) \
	((__u8)(ARG & 0x00FF))

#define GET_WORD(ARG) \
	((__u16)(ARG & 0xFFFF))


/* ######################################################################### */
/* ###################### SINGLE OPERAND INSTRUCTIONS ###################### */

static instr_ret_code_t single_op_instr_run(unibus_t *bus,
				std::function<void (ps_word_t *, addr_mode_t, pdp11_word_t *)> action16b,
				std::function<void (ps_word_t *, addr_mode_t, pdp11_byte_t *)> action8b) {
	single_op_instr_t instr = {.word = bus->instruction_read()};

	register_mode_t arg = {.byte = instr.word.low_byte()};
	pdp11_word_t *addr;

	try {
		addr = bus->host_addr_get(arg);
	}
	catch(instr_ret_code_t err) {
		return err;
	}

	if (instr.word.high_bit()) {
		action8b(&bus->ps_word, (addr_mode_t)arg.mode, (pdp11_byte_t *)addr);
	}
	else {
		action16b(&bus->ps_word, (addr_mode_t)arg.mode, addr);
	}

	return INSTR_CODE_SUCCESS;
}

template <class T>
static void clr(ps_word_t *ps_word, T *addr) {
	addr->set(0);
	ps_word->N = ps_word->V = ps_word->C = 0;
    ps_word->Z = 1;
}

static instr_ret_code_t single_op_S_instr_run(unibus_t *bus, pipeline_t *pipeline, instr_mnem_t opcode, 
				void (*action16b)(ps_word_t *, pdp11_word_t *),
				void (*action8b)(ps_word_t *, pdp11_byte_t *)) {
	InstructionCtx ctx(opcode);
	instr_ret_code_t ret = single_op_instr_run(bus,
				[&ctx, &action16b](ps_word_t *ps_word, addr_mode_t mode, pdp11_word_t *word) {
					action16b(ps_word, word);
					ctx.addStoreWordCtx(mode, word);
				},
				[&ctx, &action8b](ps_word_t *ps_word, addr_mode_t mode, pdp11_byte_t *byte) {
					action8b(ps_word, byte);
					ctx.addStoreByteCtx(mode, byte);
				});
	pipeline->evaluate(ctx);
	return ret;
}

instr_ret_code_t emu_func_CLR(unibus_t *bus, pipeline_t *pipeline) {
    return single_op_S_instr_run(bus, pipeline, instructionMnemonics::CLR, clr<pdp11_word_t>, clr<pdp11_byte_t>);
}

static instr_ret_code_t single_op_LS_instr_run(unibus_t *bus, pipeline_t *pipeline, instr_mnem_t opcode, 
				void (*action16b)(ps_word_t *, pdp11_word_t *),
				void (*action8b)(ps_word_t *, pdp11_byte_t *)) {
	InstructionCtx ctx(opcode);
	instr_ret_code_t ret = single_op_instr_run(bus,
				[&ctx, &action16b](ps_word_t *ps_word, addr_mode_t mode, pdp11_word_t *word) {
					action16b(ps_word, word);
					ctx.addLoadWordCtx(mode, word);
					ctx.addStoreWordCtx(mode, word);
				},
				[&ctx, &action8b](ps_word_t *ps_word, addr_mode_t mode, pdp11_byte_t *byte) {
					action8b(ps_word, byte);
					ctx.addLoadByteCtx(mode, byte);
					ctx.addStoreByteCtx(mode, byte);
				});
	pipeline->evaluate(ctx);
	return ret;
}

template <class T>
static void com(ps_word_t *ps_word, T *addr) {
	addr->set(~addr->get());
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
	ps_word->V = 0;
    ps_word->C = 1;
}

instr_ret_code_t emu_func_COM(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::COM, com<pdp11_word_t>, com<pdp11_byte_t>);
}

template <class T>
static void inc(ps_word_t *ps_word, T *addr) {
	addr->inc();
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
	ps_word->V = addr->is_signed_max();
}

instr_ret_code_t emu_func_INC(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::INC, inc<pdp11_word_t>, inc<pdp11_byte_t>);
}

template <class T>
static void dec(ps_word_t *ps_word, T *addr) {
	ps_word->V = addr->is_head();
	addr->dec();
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
}

instr_ret_code_t emu_func_DEC(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::DEC, dec<pdp11_word_t>, dec<pdp11_byte_t>);
}

template <class T>
static void neg(ps_word_t *ps_word, T *addr) {
	addr->set(~addr->get());
	ps_word->N = addr->is_lower_zero();
	ps_word->C = addr->is_zero();
	ps_word->V = addr->is_head();
    ps_word->C = !addr->is_zero();
}

instr_ret_code_t emu_func_NEG(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::NEG, neg<pdp11_word_t>, neg<pdp11_byte_t>);
}

template <class T>
static void tst(ps_word_t *ps_word, T *addr) {
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
	ps_word->V = 0;
    ps_word->C = 0;
}

instr_ret_code_t emu_func_TST(unibus_t *bus, pipeline_t *pipeline) {
	InstructionCtx ctx(instructionMnemonics::TST);
	instr_ret_code_t ret = single_op_instr_run(bus,
				[&ctx](ps_word_t *ps_word, addr_mode_t mode, pdp11_word_t *word) {
					tst<pdp11_word_t>(ps_word, word);
					ctx.addLoadWordCtx(mode, word);
				},
				[&ctx](ps_word_t *ps_word, addr_mode_t mode, pdp11_byte_t *byte) {
					tst<pdp11_byte_t>(ps_word, byte);
					ctx.addLoadByteCtx(mode, byte);
				});
	pipeline->evaluate(ctx);
	return ret;
}

template <class T>
static void asr(ps_word_t *ps_word, T *addr) {
	ps_word->C = addr->low_bit();
	addr->set(addr->get() >> 1);
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
	ps_word->V = ps_word->N ^ ps_word->C;
}

instr_ret_code_t emu_func_ASR(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::ASR, asr<pdp11_word_t>, asr<pdp11_byte_t>);
}

template <class T>
static void asl(ps_word_t *ps_word, T *addr) {
	ps_word->C = addr->high_bit();
	addr->set(addr->get() << 1);
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
	ps_word->V = ps_word->N ^ ps_word->C;
}

instr_ret_code_t emu_func_ASL(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::ASL, asl<pdp11_word_t>, asl<pdp11_byte_t>);
}

template <class T, typename cpu_t>
static void ror(ps_word_t *ps_word, T *addr) {
	ps_word->C = addr->low_bit();
	cpu_t data = addr->get();
	addr->set( (data >> 1) | (data << 15) );
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
	ps_word->V = ps_word->N ^ ps_word->C;
}

instr_ret_code_t emu_func_ROR(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::ROR, ror<pdp11_word_t, uint16_t>, ror<pdp11_byte_t, uint8_t>);
}

template <class T, typename cpu_t>
static void rol(ps_word_t *ps_word, T *addr) {
	ps_word->C = addr->high_bit();
	cpu_t data = addr->get();
	addr->set( (data >> 15) | (data << 1) );
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
    ps_word->V = ps_word->N ^ ps_word->C;
}

instr_ret_code_t emu_func_ROL(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::ROL, rol<pdp11_word_t, uint16_t>, rol<pdp11_byte_t, uint8_t>);
}

static const uint8_t Byte_head = 1 << 7;

instr_ret_code_t emu_func_SWAB(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::SWAB,
				[](ps_word_t *ps_word, pdp11_word_t *addr) {
					uint16_t data = addr->get();
					addr->set( (data << 8) + (data >> 8) );
					ps_word->N = !!(data & Byte_head);
                    ps_word->Z = !!addr->low_byte();
					ps_word->V = ps_word->C = 0;
				},
				[](ps_word_t *, pdp11_byte_t *) {
					assert(!"emu_func_SWAB: there is no such instruction for BYTE in PDP11!");
				});
}

template <class T>
static void adc(ps_word_t *ps_word, T *addr) {
	char C = ps_word->C;
	ps_word->V = addr->is_signed_max() && ps_word->C;
	ps_word->C = addr->is_unsigned_max() && ps_word->C;
	addr->add(C);
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
}

instr_ret_code_t emu_func_ADC(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::ADC, adc<pdp11_word_t>, adc<pdp11_byte_t>);	
}

template <class T>
static void sbc(ps_word_t *ps_word, T *addr) {
	char C = ps_word->C;
	ps_word->V = addr->is_head();
	ps_word->C = !(addr->is_zero() && ps_word->C);
	addr->sub(C);
	ps_word->N = addr->is_lower_zero();
    ps_word->Z = addr->is_zero();
}

instr_ret_code_t emu_func_SBC(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_LS_instr_run(bus, pipeline, instructionMnemonics::SBC, sbc<pdp11_word_t>, sbc<pdp11_byte_t>);	
}

instr_ret_code_t emu_func_SXT(unibus_t *bus, pipeline_t *pipeline) {
	return single_op_S_instr_run(bus, pipeline, instructionMnemonics::SWAB,
				[](ps_word_t *ps_word, pdp11_word_t *addr) {
					addr->set( ps_word->N ? -1 : 0 );
                    ps_word->Z = (ps_word->N == 0);
				},
				[](ps_word_t *, pdp11_byte_t *) {
					assert(!"emu_func_SXT: there is no such instruction for BYTE in PDP11!");
				});
}

/* ######################################################################### */
/* ######################################################################### */


/* ######################################################################### */
/* ###################### DOUBLE OPERAND INSTRUCTIONS ###################### */

static instr_ret_code_t double_op_instr_run(unibus_t *bus,
			 std::function<void (ps_word_t *, addr_mode_t, pdp11_word_t *, addr_mode_t, pdp11_word_t *)> action16b,
			 std::function<void (ps_word_t *, addr_mode_t, pdp11_byte_t *, addr_mode_t, pdp11_byte_t *)> action8b) {
	double_op_instr_t instr = {.word = bus->instruction_read()};

	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
	                src_arg = {.byte = instr.word.rshifted(6).low_byte()};

	pdp11_word_t *dst_addr, *src_addr;
	try {                
		dst_addr = bus->host_addr_get(dst_arg);
		src_addr = bus->host_addr_get(src_arg);
	}
	catch(instr_ret_code_t err) {
		return err;
	}

	if (instr.word.high_bit())
		action8b(&bus->ps_word, (addr_mode_t)dst_arg.mode, (pdp11_byte_t *)dst_addr,
			(addr_mode_t)src_arg.mode, (pdp11_byte_t *)src_addr);
	else
		action16b(&bus->ps_word, (addr_mode_t)dst_arg.mode, dst_addr, (addr_mode_t)src_arg.mode, src_addr);

	return INSTR_CODE_SUCCESS;
}

instr_ret_code_t emu_func_MOV(unibus_t *bus, pipeline_t *pipeline) {
	InstructionCtx ctx(instructionMnemonics::MOV);
	instr_ret_code_t ret = double_op_instr_run(bus,
		[&ctx](ps_word_t *ps_word, addr_mode_t dst_mode, pdp11_word_t *dst_addr, addr_mode_t src_mode, pdp11_word_t *src_addr) {
            dst_addr->set(src_addr->get());
			ps_word->N = dst_addr->is_lower_zero();
            ps_word->Z = dst_addr->is_zero();
			ps_word->V = 0;
			ctx.addLoadWordCtx(src_mode, src_addr);
			ctx.addStoreWordCtx(dst_mode, dst_addr);
		},
        [&ctx, &bus](ps_word_t *ps_word, addr_mode_t dst_mode, pdp11_byte_t *dst_addr, addr_mode_t src_mode, pdp11_byte_t *src_addr) {
			int16_t data;
            if( (unsigned char *)dst_addr < bus->memory + Emulator::ROM_size)
                throw INSTR_CODE_INVALID_ADDRESS;
			if (dst_mode == 0) /* case of MOVB to register */
				data = *(int16_t *)dst_addr = *(int8_t *)src_addr;
			else
				data = *(int8_t *)dst_addr = *(int8_t *)src_addr;

			ps_word->N = IS_LOWER_ZERO(data);
            ps_word->Z = IS_ZERO(data);
			ps_word->V = 0;
			ctx.addLoadByteCtx(src_mode, src_addr);
			ctx.addStoreByteCtx(dst_mode, dst_addr);
		});
	pipeline->evaluate(ctx);
	return ret;
}

static instr_ret_code_t double_op_LL_instr_run(unibus_t *bus, pipeline_t *pipeline, instr_mnem_t opcode, 
				void (*action16b)(ps_word_t *, pdp11_word_t *, pdp11_word_t *),
				void (*action8b)(ps_word_t *, pdp11_byte_t *, pdp11_byte_t *)) {
	InstructionCtx ctx(opcode);
	instr_ret_code_t ret = double_op_instr_run(bus,
		[&ctx, &action16b](ps_word_t *ps_word, addr_mode_t dst_mode, pdp11_word_t *dst_addr,
			addr_mode_t src_mode, pdp11_word_t *src_addr) {
			action16b(ps_word, dst_addr, src_addr);
			ctx.addLoadWordCtx(dst_mode, dst_addr);
			ctx.addLoadWordCtx(src_mode, src_addr);
		},
		[&ctx, &action8b](ps_word_t *ps_word, addr_mode_t dst_mode, pdp11_byte_t *dst_addr,
			addr_mode_t src_mode, pdp11_byte_t *src_addr) {
			action8b(ps_word, dst_addr, src_addr);
			ctx.addLoadByteCtx(dst_mode, dst_addr);
			ctx.addLoadByteCtx(src_mode, src_addr);
		});
	pipeline->evaluate(ctx);
	return ret;
}

template<class T, typename cpu_t, typename result_t>
static void cmp(ps_word_t *ps_word, T *dst_addr, T *src_addr) {
	result_t op_result = src_addr->get() + ~dst_addr->get() + (cpu_t)1;
	ps_word->N = ((cpu_t)op_result >> (sizeof(cpu_t)*8 - 1)); /* get high bit */
    ps_word->Z = op_result == 0;
	ps_word->V = src_addr->high_bit() != dst_addr->high_bit()
			  && dst_addr->high_bit() == ps_word->N;
	ps_word->C = !(op_result >> (sizeof(cpu_t)*8));
}

instr_ret_code_t emu_func_CMP(unibus_t *bus, pipeline_t *pipeline) {
	return double_op_LL_instr_run(bus, pipeline, instructionMnemonics::CMP,
		cmp<pdp11_word_t, uint16_t, uint32_t>, cmp<pdp11_byte_t, uint8_t, uint16_t>);
}

static void add(ps_word_t *ps_word, pdp11_word_t *dst_addr, pdp11_word_t *src_addr) {
	uint32_t op_result = src_addr->get() + dst_addr->get();
	dst_addr->set(GET_WORD(op_result));
	ps_word->N = GET_HIGH_BIT_WORD(op_result);
	ps_word->V = src_addr->high_bit() == dst_addr->high_bit()
		      && dst_addr->high_bit() != ps_word->N;
    ps_word->Z = IS_ZERO(op_result);
	ps_word->C = !!(op_result >> 16);
}

static void sub(ps_word_t *ps_word, pdp11_word_t *dst_addr, pdp11_word_t *src_addr) {
	uint32_t op_result = dst_addr->get() + ~src_addr->get() + (uint16_t)1;
	dst_addr->set(GET_WORD(op_result));
	ps_word->N = GET_HIGH_BIT_WORD(op_result);
	ps_word->V = src_addr->high_bit() != dst_addr->high_bit()
		     &&  dst_addr->high_bit() == ps_word->N;
    ps_word->Z = IS_ZERO(op_result);
	ps_word->C = !!(op_result >> 16);
}

instr_ret_code_t emu_func_ADD(unibus_t *bus, pipeline_t *pipeline) {
	double_op_instr_t instr = {.word = bus->instruction_read()};

	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
	                src_arg = {.byte = instr.word.rshifted(6).low_byte()};

	pdp11_word_t *dst_addr, *src_addr;
	try {                
		dst_addr = bus->host_addr_get(dst_arg);
		src_addr = bus->host_addr_get(src_arg);
	}
	catch(instr_ret_code_t err) {
		return err;
	}

	InstructionCtx ctx(instructionMnemonics::ADD);

	if (instr.word.high_bit())
		sub(&bus->ps_word, dst_addr, src_addr);
	else
		add(&bus->ps_word, dst_addr, src_addr);

	ctx.addLoadWordCtx((addr_mode_t)src_arg.mode, src_addr);
	ctx.addLoadWordCtx((addr_mode_t)dst_arg.mode, dst_addr);
	ctx.addStoreWordCtx((addr_mode_t)dst_arg.mode, dst_addr);
	pipeline->evaluate(ctx);
	return INSTR_CODE_SUCCESS;
}

template<class T, typename cpu_t>
static void bit(ps_word_t *ps_word, T *dst_addr, T *src_addr) {
	cpu_t op_result = src_addr->get() & dst_addr->get();
	ps_word->N = IS_LOWER_ZERO(op_result);
    ps_word->Z = IS_ZERO(op_result);
	ps_word->V = 0;
}

instr_ret_code_t emu_func_BIT(unibus_t *bus, pipeline_t *pipeline) {
	return double_op_LL_instr_run(bus, pipeline, instructionMnemonics::BIT,
		bit<pdp11_word_t, int16_t>, bit<pdp11_byte_t, int8_t>);
}

static instr_ret_code_t double_op_LLS_instr_run(unibus_t *bus, pipeline_t *pipeline, instr_mnem_t opcode, 
				void (*action16b)(ps_word_t *, pdp11_word_t *, pdp11_word_t *),
				void (*action8b)(ps_word_t *, pdp11_byte_t *, pdp11_byte_t *)) {
	InstructionCtx ctx(opcode);
	instr_ret_code_t ret = double_op_instr_run(bus,
		[&ctx, &action16b](ps_word_t *ps_word, addr_mode_t dst_mode, pdp11_word_t *dst_addr,
			addr_mode_t src_mode, pdp11_word_t *src_addr) {
			action16b(ps_word, dst_addr, src_addr);
			ctx.addLoadWordCtx(dst_mode, dst_addr);
			ctx.addLoadWordCtx(src_mode, src_addr);
			ctx.addStoreWordCtx(dst_mode, dst_addr);
		},
		[&ctx, &action8b](ps_word_t *ps_word, addr_mode_t dst_mode, pdp11_byte_t *dst_addr,
			addr_mode_t src_mode, pdp11_byte_t *src_addr) {
			action8b(ps_word, dst_addr, src_addr);
			ctx.addLoadByteCtx(dst_mode, dst_addr);
			ctx.addLoadByteCtx(src_mode, src_addr);
			ctx.addStoreByteCtx(dst_mode, dst_addr);
		});
	pipeline->evaluate(ctx);
	return ret;
}

template<class T>
static void bic(ps_word_t *ps_word, T *dst_addr, T *src_addr) {
	dst_addr->set(~src_addr->get() & dst_addr->get());
	ps_word->N = dst_addr->high_bit();
    ps_word->Z = dst_addr->is_zero();
	ps_word->V = 0;
}

instr_ret_code_t emu_func_BIC(unibus_t *bus, pipeline_t *pipeline) {
	return double_op_LLS_instr_run(bus, pipeline, instructionMnemonics::BIC, bic<pdp11_word_t>, bic<pdp11_byte_t>);
}

template<class T>
static void bis(ps_word_t *ps_word, T *dst_addr, T *src_addr) {
	dst_addr->set(src_addr->get() | dst_addr->get());
	ps_word->N = dst_addr->high_bit();
    ps_word->Z = dst_addr->is_zero();
	ps_word->V = 0;
}

instr_ret_code_t emu_func_BIS(unibus_t *bus, pipeline_t *pipeline) {
	return double_op_LLS_instr_run(bus, pipeline, instructionMnemonics::BIC, bis<pdp11_word_t>, bis<pdp11_byte_t>);
}

/* ######################################################################### */
/* ######################################################################### */

/* ######################################################################### */
/* ########### DOUBLE OPERAND INSTRUCTIONS REQUIRE A REGISTER ############## */

static instr_ret_code_t doubleR_op_instr_run(unibus_t *bus,
				std::function<void (addr_mode_t, pdp11_word_t *, pdp11_word_t *)> action) {
	doubleR_op_instr_t instr = {.word = bus->instruction_read()};

	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
	                reg_arg = {.byte = instr.word.rshifted(6).low_byte()};
    reg_arg.mode = Emulator::addressingModes::Register;

	pdp11_word_t *dst_addr, *reg_addr;
	try {                
		dst_addr = bus->host_addr_get(dst_arg);
		reg_addr = bus->host_addr_get(reg_arg);
	}
	catch(instr_ret_code_t err) {
		return err;
	}

	action((addr_mode_t)dst_arg.mode, dst_addr, reg_addr);
	return INSTR_CODE_SUCCESS;
}

instr_ret_code_t emu_func_XOR(unibus_t *bus, pipeline_t *pipeline) {
	InstructionCtx ctx(instructionMnemonics::XOR);
	instr_ret_code_t ret = doubleR_op_instr_run(bus,
				[&ctx, &bus](addr_mode_t dst_mode, pdp11_word_t *dst, pdp11_word_t *reg) {
					dst->set(reg->get() ^ dst->get());
					bus->ps_word.N = dst->is_lower_zero();
                    bus->ps_word.Z = dst->is_zero();
					bus->ps_word.V = 0;
					ctx.addLoadWordCtx(dst_mode, dst);
					ctx.addLoadWordCtx(Emulator::addressingModes::Register, reg);
					ctx.addStoreWordCtx(dst_mode, dst);
				});
	pipeline->evaluate(ctx);
	return ret;
}

instr_ret_code_t emu_func_ASH(unibus_t *bus, pipeline_t *pipeline) {
	InstructionCtx ctx(instructionMnemonics::ASH);
	instr_ret_code_t ret = doubleR_op_instr_run(bus,
				[&ctx, &bus](addr_mode_t num_mode, pdp11_word_t *num, pdp11_word_t *reg) {
                    uint16_t old_reg_val = reg->get();
                    uint16_t shiftN = num->rawed().get();
                    bool rshift = num->is_lower_zero();
                    
                    if (rshift) {
                        bus->ps_word.C = GET_LOW_BIT(old_reg_val >> (shiftN - 1));
                        *reg = reg->rshifted(shiftN);
                    } else {
                        bus->ps_word.C = GET_HIGH_BIT_WORD(old_reg_val << (shiftN - 1));
                        *reg = reg->lshifted(shiftN);
                    }
                    
					bus->ps_word.N = reg->is_lower_zero();
                    bus->ps_word.Z = reg->is_zero();
					bus->ps_word.V = GET_HIGH_BIT_WORD(old_reg_val) ^ GET_HIGH_BIT_WORD(reg->get());
					ctx.addLoadWordCtx(num_mode, num);
					ctx.addLoadWordCtx(Emulator::addressingModes::Register, reg);
					ctx.addStoreWordCtx(Emulator::addressingModes::Register, reg);
				});
	pipeline->evaluate(ctx);
	return ret;
}

instr_ret_code_t emu_func_JSR(unibus_t *bus, pipeline_t *pipeline) {
	InstructionCtx ctx(instructionMnemonics::JSR);
	instr_ret_code_t ret = doubleR_op_instr_run(bus,
				[&ctx, &bus](addr_mode_t dst_mode, pdp11_word_t *dst, pdp11_word_t *reg) {
                    bus->stack_push(*reg);
                    *reg = bus->PC;
                	bus->PC = bus->pdp_addr_get(dst);
					ctx.addLoadWordCtx(dst_mode, dst);
					ctx.addLoadWordCtx(Emulator::addressingModes::Register, reg);
					ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->SP);
					ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->PC);
					ctx.addStoreWordCtx(Emulator::addressingModes::Register, reg);
					ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->SP);
					ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->PC);
				});
	pipeline->evaluate(ctx);
	pipeline->reset();
	return ret;
}


/* ######################################################################### */
/* ######################################################################### */

/* ######################################################################### */
/* ########################## BRANCH INSTRUCTIONS ########################## */

static instr_ret_code_t branch_insr_run(unibus_t *bus, pipeline_t *pipeline, instr_mnem_t opcode,
 int (*decider)(ps_word_t)) {
 	InstructionCtx ctx(opcode);
	branch_op_instr_t instr = {.word = bus->instruction_read()};

    if (decider(bus->ps_word)) {
		bus->PC.add(2*(int8_t)instr.offset);
		ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->PC);
		ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->PC);
    }

    pipeline->evaluate(ctx);
    pipeline->reset();
	return INSTR_CODE_SUCCESS;
}


instr_ret_code_t emu_func_BR(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BR, [](ps_word_t )->int {return 1;});
}


instr_ret_code_t emu_func_BNE(unibus_t *bus, pipeline_t *pipeline) {
    return branch_insr_run(bus, pipeline, instructionMnemonics::BNE, [](ps_word_t ps_word)->int {return ps_word.Z == 0;});
}


instr_ret_code_t emu_func_BEQ(unibus_t *bus, pipeline_t *pipeline) {
    return branch_insr_run(bus, pipeline, instructionMnemonics::BEQ, [](ps_word_t ps_word)->int {return ps_word.Z == 1;});
}


instr_ret_code_t emu_func_BPL(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BPL, [](ps_word_t ps_word)->int {return ps_word.N == 0;});
}


instr_ret_code_t emu_func_BMI(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BMI, [](ps_word_t ps_word)->int {return ps_word.N == 1;});
}


instr_ret_code_t emu_func_BVC(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BVC, [](ps_word_t ps_word)->int {return ps_word.V == 0;});
}


instr_ret_code_t emu_func_BVS(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BVS, [](ps_word_t ps_word)->int {return ps_word.V == 1;});
}


instr_ret_code_t emu_func_BCC(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BCC, [](ps_word_t ps_word)->int {return ps_word.C == 0;});
}


instr_ret_code_t emu_func_BCS(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BCS, [](ps_word_t ps_word)->int {return ps_word.C == 1;});
}


instr_ret_code_t emu_func_BGE(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BGE,
		[](ps_word_t ps_word)->int {return (ps_word.N ^ ps_word.V) == 0;});
}


instr_ret_code_t emu_func_BLT(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BLT,
		[](ps_word_t ps_word)->int {return (ps_word.N ^ ps_word.V) == 1;});
}


instr_ret_code_t emu_func_BGT(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BGT,
		[](ps_word_t ps_word)->int {return (ps_word.Z ^ (ps_word.N ^ ps_word.V)) == 0;});
}


instr_ret_code_t emu_func_BLE(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BLE,
		[](ps_word_t ps_word)->int {return (ps_word.Z ^ (ps_word.N ^ ps_word.V)) == 1;});
}

instr_ret_code_t emu_func_BHI(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BHI,
        [](ps_word_t ps_word)->int {return (ps_word.C == 0) && (ps_word.Z == 0);});
}


instr_ret_code_t emu_func_BLOS(unibus_t *bus, pipeline_t *pipeline) {
	return branch_insr_run(bus, pipeline, instructionMnemonics::BLOS,
		[](ps_word_t ps_word)->int {return (ps_word.C ^ ps_word.Z) == 1;});
}


instr_ret_code_t emu_func_BHIS(unibus_t *bus, pipeline_t *pipeline) {
	return emu_func_BCC(bus, pipeline);
}

instr_ret_code_t emu_func_BLO(unibus_t *bus, pipeline_t *pipeline) {
	return emu_func_BCS(bus, pipeline);
}

/* ######################################################################### */
/* ######################################################################### */

/* ######################################################################### */
/* ########################### JUMP INSTRUCTIONS ########################### */

instr_ret_code_t emu_func_JMP(unibus_t *bus, pipeline_t *pipeline) {
    pdp11_word_t instr = bus->instruction_read();
    register_mode_t dst = {.byte = (uint8_t)(instr.low_byte() & (uint8_t)0b111111)};

    try {
        bus->PC = bus->pdp_addr_get(dst);
    }
   	catch(instr_ret_code_t err) {
   		return err;
   	}

    InstructionCtx ctx(instructionMnemonics::JMP);
    ctx.addLoadWordCtx((addr_mode_t)dst.mode, bus->host_addr_get(bus->PC));
    ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->PC);
   	pipeline->evaluate(ctx);
   	pipeline->reset();

    return INSTR_CODE_SUCCESS;
}

instr_ret_code_t emu_func_MARK(unibus_t *bus, pipeline_t *pipeline) {
    pdp11_word_t instr = bus->instruction_read();
    register_mode_t dst = {.byte = (uint8_t)(instr.low_byte() & (uint8_t)0b111111)};

    try {
        bus->SP.add(2*dst.byte);
        bus->PC = bus->R5;
        bus->R5 = bus->stack_pop();
    }
   	catch(instr_ret_code_t err) {
   		return err;
   	}

    InstructionCtx ctx(instructionMnemonics::MARK);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->R5);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->R5);
	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->PC);
   	pipeline->evaluate(ctx);
   	pipeline->reset();

    return INSTR_CODE_SUCCESS;
}

instr_ret_code_t emu_func_RTS(unibus_t *bus, pipeline_t *pipeline) {
    pdp11_word_t instr = bus->instruction_read();
    register_mode_t reg = {.byte = (uint8_t)(instr.low_byte() & (uint8_t)0b111)};
    reg.mode = Emulator::addressingModes::Register;
    
    pdp11_word_t *reg_addr;
    try {
        reg_addr = bus->host_addr_get(reg);
        bus->PC = *reg_addr;
        *reg_addr = bus->stack_pop();
    }
   	catch(instr_ret_code_t err) {
   		return err;
   	}

    InstructionCtx ctx(instructionMnemonics::RTS);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, reg_addr);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, reg_addr);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->SP);
	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->PC);
   	pipeline->evaluate(ctx);
   	pipeline->reset();

    return INSTR_CODE_SUCCESS;
}

/* ######################################################################### */
/* ######################################################################### */

instr_ret_code_t emu_func_RTI(unibus_t *bus, pipeline_t *pipeline) {
    bus->PC = bus->stack_pop();
    bus->ps_word.flags = bus->stack_pop();

    InstructionCtx ctx(instructionMnemonics::RTS);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addLoadWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	ctx.addStoreWordCtx(Emulator::addressingModes::Register, &bus->SP);
   	pipeline->evaluate(ctx);
   	pipeline->reset();

    return INSTR_CODE_SUCCESS;
}

instr_ret_code_t emu_func_RTT(unibus_t *bus, pipeline_t *pipeline) {
	return emu_func_RTI(bus, pipeline);
}

instr_ret_code_t emu_func_HALT(unibus_t *, pipeline_t *pipeline) {
    InstructionCtx ctx(instructionMnemonics::HALT);
   	pipeline->evaluate(ctx);
   	pipeline->reset();

    return INSTR_CODE_HALT;
}

instr_ret_code_t emu_func_WAIT(unibus_t *bus, pipeline_t *pipeline) {
    bus->instruction_read();

    InstructionCtx ctx(instructionMnemonics::WAIT);
   	pipeline->evaluate(ctx);
   	pipeline->reset();

    return INSTR_CODE_WANT_WAIT;
}

instr_ret_code_t emu_not_realized_msg() {
    return INSTR_CODE_NOT_REALISED;
}

#define EMU_CALLBACK(CMD_NAME) ((void (*)(unibus_t *))emu_func##CMD_NAME)

void emu_funcs_init(void **instr_set) {
    struct emulatorFunctions emu_funcs;
    emulatorFunctionsInit(emu_funcs);
    emu_funcs.instructionSetInit(instr_set, (void (*)())emu_not_realized_msg);
}

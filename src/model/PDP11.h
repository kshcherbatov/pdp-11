#ifndef PDP11__H
#define PDP11__H

#include <cstdint>
#include <QReadWriteLock>
#include <QStringList>

#define BIT16_CAPACITY (1 << 16)
#define GUI_NUMBERS_BASE 8
#define PDP11_TIME_PER_TICK 20

//#define DONT_HIDE_DATA_WORDS

#define DATA_WORD_PLACEHOLDER "; DATA_WORD"

typedef struct pdp11_unibus   unibus_t;
typedef class  pdp11_pic      pic_t;
typedef class  pdp11_pipeline pipeline_t;

namespace Emulator {

	const uint16_t VRAM_size = 16*1024; /* 16 KB */
    const uint16_t registerAmount = 8;
    const uint16_t instructionSize = 2;

	class PDP11 {
	    QReadWriteLock stateMutex;
	    unibus_t *unibus;
        pic_t *interruptController;
        pipeline_t *pipeline;
	    int state;
        uint32_t fakeVRAM[VRAM_size] __attribute__ ((aligned (16)));
	    void  *instr_set[BIT16_CAPACITY] __attribute__ ((aligned (16)));
	    void *disasm_set[BIT16_CAPACITY] __attribute__ ((aligned (16)));
	public:
	    struct display {
	        const size_t height;
	        const size_t width;
	    } display;
	    PDP11(const char *ROM_dump_path, bool pipelining_enabled, bool cache_enabled);
	    ~PDP11();
	    int stepi();
	    void interruptSwitchContext();
	    void makeDisasm(QStringList &disasmList);
        void restart(const char *ROM_dump_path);
        void restartPipeline(bool pipelining_enabled, bool cache_enabled);
	    int16_t Rn_get(uintptr_t n) __attribute__((const));
	    uint16_t flag_get(char flagname) __attribute__((const));
	    uint16_t flags_get()__attribute__((const));
	    const unsigned char *VRAM_get()__attribute__((const));
        void feedCharQueue(int c);
        uint64_t getClockNumber();
	};

    namespace Status {
		enum {
			Success =  0,
		    Halt    =  1,
			Wait    =  2,
		    Failed  = -1
		};
	}
}

QString snum_QString(int16_t word);

#endif // PDP11__H

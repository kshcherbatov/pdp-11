#!/usr/bin/python2

bytes = []

import sys
with open("_inthandle.tmp", "rb") as inthandle:
    byte = inthandle.read(1)
    while byte != "":
        bytes.append(byte)
        byte = inthandle.read(1)

with open(sys.argv[1], "rb+") as out:
	out.seek(256);
	out.write(bytearray(bytes))

from PIL import Image

pixel_map = {
	0   : 0b00,
	85  : 0b01,
	170 : 0b10,
	255 : 0b11
};

im = Image.open("_logo.tmp");
pix = im.load();

bytes = []

x = 0;

for i in range(40):
	for j in range(40):
		x |= pixel_map[pix[j,i]] << (2*(3 - (j%4)));
		if(j % 4 == 3):
			bytes.append(x)
			x = 0

im = Image.open("_font.tmp");
pix = im.load();

x = 0
for i in range(1312):
	for j in range(20):
		x |= pixel_map[pix[j,i]] << (2*(3 - (j%4)));
		if(j % 4 == 3):
			bytes.append(x)
			x = 0

with open(sys.argv[1], "rb+") as out:
	out.seek(1024);
	out.write(bytearray(bytes))

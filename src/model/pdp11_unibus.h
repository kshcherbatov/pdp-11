#ifndef PDP11_UNIBUS_T
#define PDP11_UNIBUS_T

extern "C" {
	#include <linux/types.h>
	#include <asm/byteorder.h>
}

#include <cstdint>
#include <QString>
#include "PDP11.h"
#include "pdp11_entry.h"

#define DEAD_WORD 0xDEAD

/* Sorry, but we must avoid inheritance & private members if we want to have POD */

typedef union {
	struct {
		__le16 C : 1;  /* carry flag */
		__le16 V : 1;  /* overflow flag */
		__le16 Z : 1;  /* zero flag */
		__le16 N : 1;  /* sign flag */
		__le16 T : 1;  /* trap flag */
		__le16 I : 11; /* interrupt priority level */
	} __attribute__((packed));
	pdp11_word_t flags;
} ps_word_t;

typedef union {
	struct {
		__u8 reg  : 3;
		__u8 mode : 3;
		__u8 pad  : 2;
	} __attribute__((packed));
	uint8_t byte;
} register_mode_t;


namespace Emulator {
    const uint16_t ROM_size = 16*1024;
    const uint16_t RAM_size = 32*1024 - 3*sizeof(pdp11_word_t) - sizeof(ps_word_t);
    const uint32_t Memory_size = 64*1024;
    const uint8_t  AddressingModesCount = 8;

    namespace addressingModes {
        enum general_register_addressing_modes {
            Register              = 0,
            RegisterDeferred      = 1,
            Autoincrement         = 2,
            AutoincrementDeferred = 3,
            Autodecrement         = 4,
            AutodecrementDeferred = 5,
            Index                 = 6,
            IndexDeferred         = 7
        };

        enum program_counter_addressing_modes {
            Immediate         = 2,
            Absolute          = 3,
            Relative          = 6,
            RelativeDeferred  = 7
        };
    }
}

typedef enum Emulator::addressingModes::general_register_addressing_modes addr_mode_t;

struct pdp11_unibus {
	/* memory */
	union {
        unsigned char memory[Emulator::Memory_size];
		struct {
            unsigned char ROM[Emulator::ROM_size];
            unsigned char RAM[Emulator::RAM_size];
            unsigned char VRAM[Emulator::VRAM_size];
            pdp11_word_t keyboard_buffer; /* buffer for scancodes from keyboard */
            pdp11_word_t interrupt_vector_PC; /* PC for interrupt handler */
            pdp11_word_t interrupt_vector_PS; /* status word for interrupt handler */
			ps_word_t ps_word; /* processor status word */
		} __attribute__((packed, aligned(8)));
	};

	/* registers */
	union {
		struct {
			/* General registers */
			pdp11_word_t R0;
			pdp11_word_t R1;
			pdp11_word_t R2;
			pdp11_word_t R3;
			pdp11_word_t R4;
			pdp11_word_t R5;

			pdp11_word_t SP; /* stack pointer   */
			pdp11_word_t PC; /* program counter */
		} __attribute__((packed));
        pdp11_word_t R[Emulator::registerAmount];
	};

	pdp11_word_t *host_addr_get(pdp11_word_t pdp_addr);
	pdp11_word_t *host_addr_get(uint16_t pdp_addr);
	pdp11_word_t *host_addr_get(register_mode_t info);
	pdp11_word_t pdp_addr_get(register_mode_t info);
    pdp11_word_t pdp_addr_get(pdp11_word_t *host_addr);
	pdp11_word_t instruction_read(); /* get instruction & inc2 PC */
    QString      addr_disasm_get(register_mode_t info);

    void stack_push(pdp11_word_t value);
    pdp11_word_t stack_pop();

    void set_high_CPU_priority() {
        ps_word.I |= 0b111;
    }
    void set_low_CPU_priority() {
        ps_word.I &= 0b11111111000;
    }
    bool CPU_priority_is_low() {
        return !(ps_word.I & 0b111);
    }
} __attribute__((packed)); /* unibus_t */

#endif // PDP11_UNIBUS_T

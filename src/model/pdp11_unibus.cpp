#include <QString>

#include "pdp11_unibus.h"
#include "instruction_set/instr_funcs.h"

using namespace Emulator;

pdp11_word_t *unibus_t::host_addr_get(pdp11_word_t pdp_addr) {
	return (pdp11_word_t *)(memory + pdp_addr.get());
	// return (pdp11_word_t *)memory + (pdp_addr.get() >> 1);
}

pdp11_word_t unibus_t::pdp_addr_get(pdp11_word_t *host_addr)
{
    if((unsigned char *)host_addr >= memory + Memory_size ||
		(unsigned char *)host_addr < memory)
		throw INSTR_CODE_INVALID_ADDRESS;
    pdp11_word_t res;
    res.set((unsigned char *)host_addr - memory);
    
    return res;
}

pdp11_word_t *unibus_t::host_addr_get(uint16_t pdp_addr) {
	return (pdp11_word_t *)(memory + pdp_addr);
}

pdp11_word_t unibus_t::instruction_read() {
	pdp11_word_t instr = *this->host_addr_get(this->PC);
	this->PC.inc2();
	return instr;
}

static pdp11_word_t pdp_addr_get_general(unibus_t *bus, register_mode_t info) {
    using namespace addressingModes;

	pdp11_word_t pdp_addr;

	switch (info.mode) {
        case Register:
			throw INSTR_CODE_INVALID_MODE;
			break;
        case RegisterDeferred:
			pdp_addr = bus->R[info.reg];
			break;
        case Autoincrement:
			pdp_addr = bus->R[info.reg];
			bus->R[info.reg].inc();
			break;
        case AutoincrementDeferred:
			pdp_addr = *bus->host_addr_get(bus->R[info.reg]);
			bus->R[info.reg].inc2();
			break;
        case Autodecrement:
			bus->R[info.reg].dec();
			pdp_addr = bus->R[info.reg];
			break;
        case AutodecrementDeferred:
			bus->R[info.reg].dec2();
			pdp_addr = *bus->host_addr_get(bus->R[info.reg]);
			break;
        case Index:
			pdp_addr = bus->instruction_read();
			pdp_addr.add(bus->R[info.reg]);
			break;
        case IndexDeferred: {
			pdp11_word_t pdp_addr_of_addr = bus->instruction_read();
			pdp_addr_of_addr.add(bus->R[info.reg]);
			pdp_addr = *bus->host_addr_get(pdp_addr_of_addr);
			break;
		}
	}

	return pdp_addr;
}

static pdp11_word_t pdp_addr_get_SP(unibus_t *bus, register_mode_t info) {
    using namespace addressingModes;

	pdp11_word_t pdp_addr;

	switch (info.mode) {
        case Autoincrement:
		 	/* The operand is on the top of the stack, then pop it off */
			pdp_addr = bus->SP;
            bus->SP.inc2();
			break;
        case Autodecrement:
		 	/* Push a value onto the stack */
			bus->SP.dec2();
			pdp_addr = bus->SP;
			break;
		default:
			pdp_addr = pdp_addr_get_general(bus, info);
			break;
	}

	return pdp_addr;
}

static pdp11_word_t pdp_addr_get_PC(unibus_t *bus, register_mode_t info) {
    using namespace addressingModes;

    pdp11_word_t pdp_addr;

	switch (info.mode) {
        case Immediate:
			/* The operand is contained in the instruction */
			pdp_addr = bus->PC;
			bus->PC.inc2();
			break;
        case Absolute:
			/* The absolute address is contained in the instruction;
			 * the same as AUTOINCREMENT_DEFERRED (equal numerically) for general register */
        case Relative:
			/* An extra word in the instruction is added to PC+2 to give the address;
			 * the effect is the same as applying INDEX general mode (equal numerically) */
        case RelativeDeferred:
			/* An extra word in the instruction is added to PC+2 to give the address of the address;
			 * the effect is the same as applying INDEX_DEFERRED general mode (equal numerically) */
			pdp_addr = pdp_addr_get_general(bus, info); /* Don't repeat yourself */
			break;
		default:
			throw INSTR_CODE_INVALID_MODE;
			break;
	}

	return pdp_addr;
}

pdp11_word_t unibus_t::pdp_addr_get(register_mode_t info) {
	switch(info.reg) {
		case 6: /* Stack pointer */
			return pdp_addr_get_SP(this, info);
		case 7: /* Program counter */
			return pdp_addr_get_PC(this, info);
		default:
			return pdp_addr_get_general(this, info);
	}
}

pdp11_word_t *unibus_t::host_addr_get(register_mode_t info) {
    using namespace addressingModes;

    pdp11_word_t *addr = 0;

	switch (info.mode) {
        case Register:
			addr = &this->R[info.reg];
			break;
		default:
			addr = this->host_addr_get(this->pdp_addr_get(info));
			break;
	}

	return addr;
}

void unibus_t::stack_push(pdp11_word_t value) {
    if(SP.get() > (RAM - memory + RAM_size))
        throw INSTR_CODE_INVALID_ADDRESS;
    if(SP.get() < (uint16_t)(RAM - memory))
        throw INSTR_CODE_INVALID_ADDRESS;
    if(SP.get() == (uint16_t)(RAM - memory))
        throw INSTR_CODE_STACK_OVERFLOW;

    SP.dec2();
    pdp11_word_t *stack =  host_addr_get(SP);
    *stack = value;
}

pdp11_word_t unibus_t::stack_pop() {
    if(SP.get() > (RAM - memory + RAM_size))
        throw INSTR_CODE_INVALID_ADDRESS;
    if(SP.get() < (uint16_t)(RAM - memory))
        throw INSTR_CODE_INVALID_ADDRESS;
    if(SP.get() == (RAM - memory + RAM_size))
        throw INSTR_CODE_EMPTY_STACK;

    pdp11_word_t *stack = host_addr_get(SP);
    pdp11_word_t res = *stack;
    (*stack).set(0666);
    SP.inc2();
    return res;
}

#define REGISTER_MODES_DISAS_SET(R) {\
	      #R,      /* REGISTER */ \
	  "(" #R ")",  /* REGISTER_DEFERRED */ \
	  "(" #R ")+", /* AUTOINCREMENT */ \
	 "@(" #R ")+", /* AUTOINCREMENT DEFERRED */ \
	 "-(" #R ")",  /* AUTODECREMENT */ \
	"@-(" #R ")" /* AUTODECREMENT DEFERRED */ \
}

static QString instr_disas_get(unibus_t *bus) {
    return snum_QString(bus->instruction_read().get());
}

static const char *addr_disas[registerAmount][AddressingModesCount - 2] = {
	REGISTER_MODES_DISAS_SET(R0),
	REGISTER_MODES_DISAS_SET(R1),
	REGISTER_MODES_DISAS_SET(R2),
	REGISTER_MODES_DISAS_SET(R3),
	REGISTER_MODES_DISAS_SET(R4),
	REGISTER_MODES_DISAS_SET(R5),
	REGISTER_MODES_DISAS_SET(SP),
	REGISTER_MODES_DISAS_SET(PC)
};

static const char *addr_index_disas[] = {"(R0)" "(R1)", "(R2)", "(R3)", "(R4)", "(R5)", "(SP)"};

static QString addr_disasm_get_general(unibus_t *bus, register_mode_t info) {
    using namespace addressingModes;

	switch(info.mode) {
        case Index:
			return instr_disas_get(bus) + addr_index_disas[info.reg];
        case IndexDeferred:
			return "@" + instr_disas_get(bus) + addr_index_disas[info.reg];
		default:
			return QString(addr_disas[info.reg][info.mode]);
	}
}

static QString addr_disasm_get_PC(unibus_t *bus, register_mode_t info) {
    using namespace addressingModes;

	switch (info.mode) {
        case Immediate:
			return "#" + instr_disas_get(bus);
        case Absolute:
			return "@#" + instr_disas_get(bus);
        case Relative:
			return instr_disas_get(bus);
        case RelativeDeferred:
			return "@" + instr_disas_get(bus);
		default:
            return addr_disasm_get_general(bus, info);
	}
}

QString unibus_t::addr_disasm_get(register_mode_t info) {
	if(info.reg != 7)
		return addr_disasm_get_general(this, info);
	else
        return addr_disasm_get_PC(this, info);
}

#include <assert.h>
#include <stddef.h>

#include "instr_set_init.h"
#include "instr_funcs.h"
#include "../pdp11_unibus.h"


/* ######################################################################### */
/* ###################### SINGLE OPERAND INSTRUCTIONS ###################### */

static QString single_op_instr_disasm_addr(unibus_t *bus) {
	single_op_instr_t instr = {.word = bus->instruction_read()};
	register_mode_t arg = {.byte = instr.word.low_byte()};

	QString cmdType = (instr.word.high_bit()) ? "B" : "";
	return cmdType + " " + bus->addr_disasm_get(arg);
}

QString disasm_func_CLR(unibus_t *bus) {
	return "CLR" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_COM(unibus_t *bus) {
	return "COM" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_INC(unibus_t *bus) {
	return "INC" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_DEC(unibus_t *bus) {
	return "DEC" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_NEG(unibus_t *bus) {
	return "NEG" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_TST(unibus_t *bus) {
	return "TST" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_ASR(unibus_t *bus) {
	return "ASR" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_ASL(unibus_t *bus) {
	return "ASL" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_ROR(unibus_t *bus) {
	return "ROR" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_ROL(unibus_t *bus) {
	return "ROL" + single_op_instr_disasm_addr(bus);
}

QString disasm_func_SWAB(unibus_t *bus) {
	return "SWAB" + single_op_instr_disasm_addr(bus);
}

QString disasm_func_ADC(unibus_t *bus) {
	return "ADC" + single_op_instr_disasm_addr(bus);
}


QString disasm_func_SBC(unibus_t *bus) {
	return "SBC" + single_op_instr_disasm_addr(bus);
}

QString disasm_func_SXT(unibus_t *bus) {
	return "SXT" + single_op_instr_disasm_addr(bus);
}

/* ######################################################################### */
/* ######################################################################### */


/* ######################################################################### */
/* ###################### DOUBLE OPERAND INSTRUCTIONS ###################### */

static QString double_op_instr_disasm_addr(unibus_t *bus) {
	double_op_instr_t instr = {.word = bus->instruction_read()};
	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
		            src_arg = {.byte = instr.word.rshifted(6).low_byte()};

	QString cmdType = (instr.word.high_bit()) ? "B" : "";
	return cmdType + " " + bus->addr_disasm_get(src_arg) + ", " + bus->addr_disasm_get(dst_arg);
}

QString disasm_func_MOV(unibus_t *bus) {
	return "MOV" + double_op_instr_disasm_addr(bus);
}


QString disasm_func_CMP(unibus_t *bus) {
	return "CMP" + double_op_instr_disasm_addr(bus);
}

QString disasm_func_ADD(unibus_t *bus) {
	double_op_instr_t instr = {.word = bus->instruction_read()};
	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
		            src_arg = {.byte = instr.word.rshifted(6).low_byte()};

	QString cmdType = (instr.word.high_bit()) ? "SUB" : "ADD";
	return cmdType + " " + bus->addr_disasm_get(src_arg) + ", " + bus->addr_disasm_get(dst_arg);
}

QString disasm_func_BIT(unibus_t *bus) {
	return "BIT" + double_op_instr_disasm_addr(bus);
}

QString disasm_func_BIC(unibus_t *bus) {
	return "BIC" + double_op_instr_disasm_addr(bus);
}


QString disasm_func_BIS(unibus_t *bus) {
	return "BIS" + double_op_instr_disasm_addr(bus);
}

/* ######################################################################### */
/* ######################################################################### */

/* ######################################################################### */
/* ########### DOUBLE OPERAND INSTRUCTIONS REQUIRE A REGISTER ############## */

static QString doubleR_op_instr_disasm_addr(unibus_t *bus, bool revert_order = false) {
	doubleR_op_instr_t instr = {.word = bus->instruction_read()};

	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
            reg_arg = {.byte = instr.word.rshifted(6).low_byte()};
    reg_arg.mode = Emulator::addressingModes::Register;

    if (revert_order) {
        register_mode_t tmp = dst_arg;
        dst_arg = reg_arg;
        reg_arg = tmp;
    }
	return QString(" ") + bus->addr_disasm_get(reg_arg) + ", " + bus->addr_disasm_get(dst_arg);
}

QString disasm_func_XOR(unibus_t *bus) {
	return "XOR" + doubleR_op_instr_disasm_addr(bus);
}

QString disasm_func_ASH(unibus_t *bus) {
    return "ASH" + doubleR_op_instr_disasm_addr(bus, true);
}

QString disasm_func_JSR(unibus_t *bus) {
    doubleR_op_instr_t instr = {.word = bus->instruction_read()};

	register_mode_t dst_arg = {.byte = instr.word.low_byte()},
            reg_arg = {.byte = instr.word.rshifted(6).low_byte()};
    reg_arg.mode = Emulator::addressingModes::Register;
    
    return "JSR " + bus->addr_disasm_get(reg_arg) + ", " 
            + QString("%1").arg(bus->pdp_addr_get(dst_arg).get(), 0, GUI_NUMBERS_BASE, QChar('0'));
}


/* ######################################################################### */
/* ######################################################################### */

/* ######################################################################### */
/* ########################## BR5ANCH INSTRUCTIONS ########################## */

static QString branch_insr_disasm_addr(unibus_t *bus) {
	branch_op_instr_t instr = {.word = bus->instruction_read()};
	int8_t value = 2*instr.offset;
    return snum_QString(value) +
            QString("  ;<0%1>").arg(bus->PC.get() + 2*instr.offset,
                                    0, GUI_NUMBERS_BASE, QChar('0'));
}

QString disasm_func_BR(unibus_t *bus) {
	return "BR " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BNE(unibus_t *bus) {
	return "BNE " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BEQ(unibus_t *bus) {
	return "BEQ " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BPL(unibus_t *bus) {
	return "BPL " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BMI(unibus_t *bus) {
	return "BMI " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BVC(unibus_t *bus) {
	return "BVC " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BVS(unibus_t *bus) {
	return "BVS " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BCC(unibus_t *bus) {
	return "BCC " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BCS(unibus_t *bus) {
	return "BCS " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BGE(unibus_t *bus) {
	return "BGE " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BLT(unibus_t *bus) {
	return "BLT " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BGT(unibus_t *bus) {
	return "BGT " + branch_insr_disasm_addr(bus);
}

QString disasm_func_BLE(unibus_t *bus) {
	return "BLE " + branch_insr_disasm_addr(bus);
}

QString disasm_func_BHI(unibus_t *bus) {
	return "BHI " + branch_insr_disasm_addr(bus);
}

QString disasm_func_BLOS(unibus_t *bus) {
	return "BLOS " + branch_insr_disasm_addr(bus);
}


QString disasm_func_BHIS(unibus_t *bus) {
	return "BHIS " + branch_insr_disasm_addr(bus);
}

QString disasm_func_BLO(unibus_t *bus) {
	return "BLO " + branch_insr_disasm_addr(bus);
}

/* ######################################################################### */
/* ######################################################################### */

/* ######################################################################### */
/* ########################### JUMP INSTRUCTIONS ########################### */

QString disasm_func_JMP(unibus_t *bus) {
    pdp11_word_t instr = bus->instruction_read();
	register_mode_t dst = {.byte = (uint8_t) (instr.low_byte() & (uint8_t) 0b111111)};

	pdp11_word_t addr = bus->pdp_addr_get(dst);

	return QString("JMP 0%1").arg(addr.get(), 0, 
                                  GUI_NUMBERS_BASE, QChar('0'));
}

QString disasm_func_MARK(unibus_t *bus) {
    pdp11_word_t instr = bus->instruction_read();
	register_mode_t dst = {.byte = (uint8_t) (instr.low_byte() & (uint8_t) 0b111111)};
    
	return QString("MARK 0%1").arg(dst.byte, 0, 
                                  GUI_NUMBERS_BASE, QChar('0'));
}

QString disasm_func_RTS(unibus_t *bus) {
	pdp11_word_t instr = bus->instruction_read();
	register_mode_t reg = {.byte = (uint8_t) (instr.low_byte() & (uint8_t) 0b111)};
    reg.mode = Emulator::addressingModes::Register;
    
	return "RTS " + bus->addr_disasm_get(reg);
}


/* ######################################################################### */
/* ######################################################################### */

QString disasm_func_HALT(unibus_t *bus) {
	bus->PC.inc2();
	return "HALT";
}

QString disasm_func_WAIT(unibus_t *bus) {
    bus->PC.inc2();
    return "WAIT";
}

QString disasm_func_RTI(unibus_t *bus) {
    bus->PC.inc2();
    return "RTI";
}

QString disasm_func_RTT(unibus_t *bus) {
    bus->PC.inc2();
    return "RTT";
}

QString disasm_unknown_instruction(unibus_t *bus) {
	pdp11_word_t instr = bus->instruction_read();
	return QString("UNKNOWN INSTR (0%1)").arg(instr.get(), 0,
                                              GUI_NUMBERS_BASE, QChar('0'));
}

#define EMU_CALLBACK(CMD_NAME) ((void (*)(unibus_t *))disasm_func##CMD_NAME)


void disasm_funcs_init(void **instr_set) {
    emulatorFunctions emu_funcs;
    emulatorFunctionsInit(emu_funcs);
    emu_funcs.instructionSetInit(instr_set, (void (*)()) disasm_unknown_instruction);
}

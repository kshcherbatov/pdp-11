#include <assert.h>
#include "instr_funcs.h"
#include "instr_set_init.h"

const uint16_t INSTR_BFLAG = 1 << 15;

void emulatorFunctions::instructionSetInit(void **instr_set, void (*default_func)()) {
#if __GNUC__ > 4
#pragma omp parallel for simd aligned(instr_set: 16)
#else
#pragma omp parallel for
#endif
	for (ptrdiff_t i = 0; i < BIT16_CAPACITY; i++)
        instr_set[i] = (void *)default_func;

#pragma omp parallel for
    for (uintptr_t i = 0; i < emu_func_num; i++) {
        uint16_t opcode_base = info[i].opcode << info[i].opcode_shift;
        void *emu_func = (void *)info[i].func;
        ptrdiff_t op_arg_sup = 1 << info[i].opcode_shift;

#if __CNUC__ > 4
#pragma omp simd aligned(instr_set: 16)
#endif
		for (ptrdiff_t arg = 0; arg < op_arg_sup; arg++)
			instr_set[opcode_base + arg] = emu_func;

        if (info[i].has_bit_mode) {
#if __CNUC__ > 4
#pragma omp simd aligned(instr_set: 16)
#endif
			for (ptrdiff_t arg = 0; arg < op_arg_sup; arg++)
				instr_set[(opcode_base | INSTR_BFLAG) + arg] = emu_func;
		}
	}
}
